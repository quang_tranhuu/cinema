<?php include 'views/layout/header1.php'; ?>
</header>
<!-- BEGIN .content -->
<section class="content" style="padding-top: 0px">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

        <!-- BEGIN .without-sidebar-layout -->
        <div class="without-sidebar-layout">
            <form action="<?php echo DOMAIN;?>index.php?controller=choice&action=form_fill" method="POST" id="choseseat">
                <input id="seat_name" type="hidden" name="seat_name" value="">
                <input id="id_seat" type="hidden" name="id_seat" value="">
                <input id="number_ticket" type="hidden" name="number" value="<?php echo  $_SESSION['quantity_ticket']; ?>">
                <div class="content-panel">
                    <div class="panel-title">
                        <h2>Thông tin </h2>
                    </div>

                    <div class="panel-block">
                        <div class="row">
                            <div class="col-md-5">
                            <!--<div class="thongtin">-->
                            <p><strong>Rạp phim : </strong> <span><?php echo $_SESSION['cinema_name']; ?></span></p><span></span>
                            <p><strong>Tên phim : </strong><span><?php echo $_SESSION['film_name']; ?></span></p><span></span>
                            <p><strong>Ngày chiếu : </strong> <span><?php echo  $_SESSION['day_name']; ?></span></p><span></span>
                            <p><strong>Suất chiếu : </strong><span><?php echo  $_SESSION['time_name']; ?></span></p><span></span>
                            <p><strong>Số lượng vé: </strong> <span><?php echo  $_SESSION['quantity_ticket']; ?></span></p><span></span>
                            <p><strong>Tổng tiền : </strong> <span><?php echo  $_SESSION['total_price']; ?> VNĐ</span></p><span></span>
                            </div>
                            <div class="col-md-7">
                                <!--<img src="">-->
                            </div>
                        </div>

                    </div>
                </div>
                <div class="content-panel" >
                    <div class="panel-title">
                        <h2>Chọn vị trí ghế </h2>
                    </div>
                        <div class="panel-block" id="chon-ghe" style="width: 55%;margin: auto">
                        <ul>
                            <li><div class="btn btn-default"></div>Ghế trống</li>
                            <li><div class="btn btn-danger"></div> Ghế có ng đặt</li>
                            <li><div class="btn btn-primary"></div>Ghế đã chọn</li>
                        </ul>
                        <img src="<?php echo URL_IMAGE?>screen.png" style="margin-left: 46px;margin-bottom: 20px">
                        <div class="clearfix">

                        </div>
                        <?php foreach ($list_seats as $value) { ?>
                        <input type="button" onclick="selectSeat(<?php echo $value['id']; ?>)" id="<?php echo $value['id']; ?>" name="id_seat" class="btn <?php echo $value['stats'] ? 'btn-danger' : 'btn-default' ?>" 
                               placeholder="<?php echo $value['stats'] ?>" value="<?php echo $value['name'] ?>" style="width: 50px;margin-bottom: 5px;">
                        <?php } ?>
                        <button  onclick="Continue()" type="button" id="ok">OK</button>
                        <a href="<?php echo DOMAIN?>index.php?controller=choice&action=list_cinema" class="btn btn-danger" style="display: none" id="ql">Đổi xuất chiếu</a>
                        <input type="submit" value="Tiếp tục" style="display: none" id="tt">
                    </div>
                
                </div>
            </form>
        </div>
    </div>
</section>
            <script>
    var numberSeat = $("#number_ticket").attr('value');
    var seatCount = 0;
    var _listSeatName = "";
    var _listSeatID="";

//    alert(numberSeat);
    function selectSeat(seatID) {
        // xử lý check chọn ghế

        var _value = $("#" + seatID).attr('class');
        if (_value == "btn btn-default") {
            $("#" + seatID).attr('class', 'btn btn-primary');
            seatCount += 1;
        }
        else if (_value == "btn btn-primary") {
            $("#" + seatID).attr('class', 'btn btn-default');
            seatCount -= 1;
        }
        else if (_value == "btn btn-danger") {
            alert('ghế đã có người đặt');
        }          

        if (seatCount > numberSeat) {
            var _classSeat = $("#" + seatID).attr('class');
            if (_classSeat == "btn btn-primary") {
                $("#" + seatID).attr('class', 'btn btn-default');
                seatCount -= 1;
            }
            alert('You only select ' + parseFloat(numberSeat) + ' Seat');
            return false;
        }

    };
    function GetListSeat() {
        var a = $("input");
        
        a.each(function () {
            if ($(this).attr('class') == "btn btn-primary") {
                _listSeatName += $(this).attr('value') + ",";
                _listSeatID += $(this).attr('id') + ",";

            }
        });
    };
    function Continue() {
        
        if (seatCount < numberSeat) {
            alert('Bạn phải chọn thêm' + (numberSeat - seatCount) + 'ghế !');
            return false;
        }
        GetListSeat();        
        if (_listSeatName != "" && _listSeatID!="") {
            $("#seat_name ").attr('value', _listSeatName);
            $("#id_seat ").attr('value', _listSeatID);
            $("button#ok").hide();
            $("input#tt").show();
            $("#ql").show();
        }
        else {
            alert('Please select seat');
            return false;
        }

    };
    
</script>    
<?php include 'views/layout/footercinema.php'; ?>