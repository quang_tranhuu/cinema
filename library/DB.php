<?php

/**
 * Description of DB
 *
 * @author Administrator
 */


class DB {
    
    static $db;
    
    static function connect() {
        $dsn = 'mysql:host='.DB_HOST.':3300; dbname='.DB_NAME;
        self::$db = new PDO($dsn, DB_USER, DB_PASSWORD);
        
        self::$db->query("SET NAMES utf8"); 
    }
    static function query_array($query='') {
        $result=  self::$db->query($query);
        return $result;
        
    }
    
    static function checkTypeImage($file){
        switch ($file) {
            case 'image/jpeg':
                return '.jpg';
                break;
            case 'image/png':
                return '.png';
                break;
            default:
                break;
        }
    }

    static function query_list($query = '') {
//        print_r($query);
//        exit;
        $result = self::$db->query($query);
        
        $list = array();
        if(!empty($result))
        while ($row = $result->fetch()) {
            $list[] = $row;
        }

        return $list;
    }
     static function get_count($table) {
        $res = DB::get_record('SELECT COUNT(id) AS total FROM '.$table);
        return $res['total'];
    }
    static function get_count_where($table,$where) {
        $res = DB::get_record("SELECT COUNT(id) AS total FROM ".$table." WHERE status='$where'");
        return $res['total'];
    }
    static function check_session($field,$default){
        if(isset($_SESSION[$field])&&$_SESSION[$field]!=''){
            return $default;
        }  else {
            return '';
        }
    }
    static function get_session($field,$default){
        if(isset($_SESSION[$field])){
            return $_SESSION[$field];
        }  else {
            return $default;
        }
    }
    static function count_table($table){
        $count = DB::get_record('SELECT COUNT(id) AS total FROM '.$table);
        return $count['total'];
    }

    static function get_list_limit_record($limit = 10, $offset = 0,$table,$where) {
        $list= DB::query_list("SELECT * FROM ".$table
                            ." WHERE status= '$where' "
                            . " LIMIT $offset, $limit");
           
            return $list;
    }
    static function get_post($input_name, $default = NULL) {
            if (isset($_POST[$input_name])) {
                return $_POST[$input_name];
            } else {

                return $default;
            }
    }
    static function get_list_limit_user($limit = 10, $offset = 0,$table,$where) {
        $list= DB::query_list("SELECT * FROM ".$table
                            ." WHERE role= '$where' "
                            . " LIMIT $offset, $limit");
            
            return $list;
    }
//    static function get_errors($input_name, $default = NULL) {
//            if (isset($errors[$input_name])) {
//                return $errors[$input_name];
//            } else {
//
//                return $default;
//            }
//    }
//    static function get_file($input_name, $default = NULL) {
//            if (isset($_FILES[$input_name])) {
//                
//                return $_FILES[$input_name];
//            } else {
//                
//                return $default;
//            }
//    }
    static function get_get($input_name, $default = NULL) {
            if (isset($_GET[$input_name])) {
                return $_GET[$input_name];
            } else {

                return $default;
            }
    }
    static function get($table, $id) {
        $result = self::$db->query("SELECT * FROM $table WHERE id =$id");    
        return $result->fetch();
    }
    /**
     * Get record array by query
     * @param string $query
     * @return array
     */
    static function get_record($query = '') {
        $result = self::$db->query($query);    
        return $result->fetch();
    }
    
    static function get_where($table, $where = []) {
        $query = "SELECT * FROM $table WHERE ";
        
        $arr = [];
        foreach($where as $key => $value) {
            $arr[] = " $key = '$value' ";
        }
        
        $query .= implode(' AND ', $arr);
        $result = self::$db->query($query);// có thay đổi getwhere từ lấy 1 bản ghi bằng lấy 1 array bằng cách thêm $list
//        print_r($result);
//        exit;
        $list = array();
        while ($row = $result->fetch()) {
            $list[] = $row;
        }
        return $list;
//        return $result->fetch();
    }
    static function get_col_where($table, $where = []) {
        $query = "SELECT suat,id FROM $table  WHERE ";
        
        $arr = [];
        
        foreach($where as $key => $value) {
            $arr[] = " $key = '$value' ";
        }
        
        $query .= implode(' AND ', $arr);
        $query .="ORDER BY suat ASC";
//        print_r($query);
//        exit;
        $result = self::$db->query($query);
        $list = array();
        
        while ($row = $result->fetch()) {
            $list[] = $row;
        }
        return $list;
    }
    
    static function delete($table, $id) {
        self::$db->query("DELETE FROM $table WHERE id = $id");
    }
    static function query($query){
        self::$db->query($query);
    }

        /**
     * Insert data to table
     * @param type $table
     * @param type $data
     */
    static function insert($table, $data) {
        $query = "INSERT INTO $table(".implode(',', array_keys($data)).") VALUES(";
        $values = [];
        foreach($data as $value) {
            $values[] = "'$value'";
        }
        
        $query .= implode(',', $values);
        $query .= ")"; 
        self::$db->query($query);
    }
    static function update($table, $data,$id) {
        $query1="UPDATE  $table SET ";
        $value1 = [];
        foreach ($data as $key => $value) {
            $value1[]="$key='$value'";
        }
        $query1 .=implode(',', $value1);
        $query1 .= " WHERE id=$id"; 
//        print_r($query1);
//        exit;
        self::$db->query($query1);
    }
    static function sendMail($cc,$content,$title) {
         require_once 'PEAR_Mail/Mail.php';
            $options = array();
            $options['host'] = 'ssl://smtp.gmail.com';
            $options['port'] = 465;
            $options['auth'] = TRUE;
            $options['username'] = 'hdcinema.com';
            $options['password'] = '01225527432';
            $mail = Mail::factory('smtp', $options);
            $headers = array(
                'From' => 'hdcinema.com@gmail.com',
                'To' => $cc,
                'Subject' => $title,
                'Content-type' => 'text/html'
            );
            $reciptions = $cc;

            $mesage = $content;

            $result = $mail->send($reciptions, $headers, $mesage);
            if (PEAR::isError($result)) {
                echo $result->getMessage();
            }
        
    }
}