
<!-- BEGIN html -->
<html lang = "en">
    <!-- BEGIN head -->
    <head>
        <!-- Meta Tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>HDcinema | Booking ticket online</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo URL_IMAGE?>favicon4.ico" type="image/x-icon" />

        <!-- Stylesheets -->
        <link rel="stylesheet" href="<?php echo DOMAIN ?>public/css/bootstrap.min.css"/>
        <!--<script src="<?php echo DOMAIN ?>public/js/jquery-1.12.2.min.js"></script>-->
        <script src="<?php echo DOMAIN ?>public/js/bootstrap.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>reset.css" />
        <link type='text/css' rel='stylesheet' href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,700' />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>ot-menu.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>main-stylesheet.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>shortcodes.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>responsive.css" />
        <script src="<?php echo DOMAIN ?>public/js/jquery-1.12.2.min.js"></script>
        <!--[if lte IE 8]>
        <link type="text/css" rel="stylesheet" href="css/ie-ancient.css" />
        <![endif]-->
        <meta property="fb:app_id" content="{1032585593498383}" />

        <!-- Demo Only -->
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>demo-settings.css" />

        <!-- END head -->
    </head>

    <!-- BEGIN body -->
    <body>
        <div id="fb-root"></div>
        <script>
            (function (d, s, id) {
                var js, fjs = d.getElementsByTagName(s)[0];
                if (d.getElementById(id))
                    return;
                js = d.createElement(s);
                js.id = id;
                js.src = "//connect.facebook.net/vi_VN/sdk.js#xfbml=1&version=v2.6&appId=1032585593498383";
                fjs.parentNode.insertBefore(js, fjs);
            }(document, 'script', 'facebook-jssdk'));</script>
        <!-- BEGIN .boxed -->
        <div class="boxed">
            <!-- BEGIN .header -->
            <header class="header light">
<!--                <div class="header-upper">
                     BEGIN .wrapper 
                    <div class="wrapper">
                        <ul class="right">
                            <li><a href="#"><i class="fa fa-facebook"></i></a></li>
                            <li><a href="#"><i class="fa fa-twitter"></i></a></li>
                            <li><a href="#"><i class="fa fa-google-plus"></i></a></li>
                            <li><a href="#"><i class="fa fa-youtube-play"></i></a></li>
                            <li><a href="#"><i class="fa fa-vimeo-square"></i></a></li>
                        </ul>
                        <div class="clear-float"></div>
                         END .wrapper 
                    </div>
                </div>-->

                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <!-- BEGIN #main-menu -->
                    <nav id="main-menu">
<!--                        <div class="search-head">
                            <form action="#">
                                <input type="text" class="ot-search-field" value="" />
                                <input type="submit" class="ot-search-button" value="Search" />
                            </form>
                        </div>-->
                        <ul class="top-menu ot-menu-add" style="background-color: rgb(41, 128, 185);">
                            <li><a href="<?php echo DOMAIN ?>index.php?controller=choice&action=list_cinema">Lịch chiếu</a></li>
                            <li><a href=""><span>Phim</span></a>
                                <ul>
                                    <li><a href="">Phim đang chiếu</a></li>
                                    <li><a href="">Phim sắp chiếu</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="">Rạp phim</a>
                            </li>
                            <li><a href="">Khuyến mãi</a></li>
                            <li><a href="contact-us.html">Liên hệ</a></li>
                            <li class="welcome" style="display: <?php echo DB::check_session('username', 'none')?>">
                                <a href="<?php echo DOMAIN?>index.php?controller=auth&action=login">Login</a></li>
                            <li class="welcome" style="display: <?php echo DB::check_session('username', 'none')?>">
                                <a href="">Register</a></li>
                            <li class="logined" style="display: <?php echo isset($_SESSION['username'])? '' :'none';?>" >
                                <div class="top-right">
                                    Welcome <b><?php echo isset($_SESSION['username'])? $_SESSION['username']:'' ?></b> | <a href="<?php echo DOMAIN . 'index.php?controller=auth&action=logout' ?>" style="color: black">Logout</a>
                                </div>
                            </li>
                        </ul>
                        <!-- END #main-menu -->
                    </nav>
                    <!-- END .wrapper -->
                </div>