<?php

class User_model {
    
    function get_user($username = '') 
    {
        return DB::get_where('users', [
            'username' => $username
        ]);
    }
    
}