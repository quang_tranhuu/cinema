<?php
session_start();
include '../config.php';
require_once '../library/DB.php';
DB::connect();

$controller = isset($_GET['controller']) ? $_GET['controller'] : 'homepage';

if ($controller != 'auth' && empty($_SESSION['user_id'])) {
   
    header('Location: '.DOMAIN.'admin/index.php?controller=auth&action=login');
}

$action = isset($_GET['action']) ? $_GET['action'] : 'index';

include('controllers/' . $controller .'.php');

