<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Film_model
 *
 * @author tran
 */
class Film_model {
    static function get_list() {
        $result = DB::query_list('SELECT * FROM films ');
        return $result;
    }
    static function get($table, $id) {
        $result = DB::query_list('SELECT * FROM'. $table.' WHERE category_id ='. $id);    
        return $result;
    }
    static function get_by_id($id){
        $result=DB::query_list('SELECT * FROM films WHERE id='.$id);
        return $result;
    }
    static function get_by_idcategory($idcate){
        $result=DB::query_list('SELECT * FROM films WHERE category_id='.$idcate);
        return $result;
    }
    
    /**
     * Get list film high view     
     * @param int $limit
     * @return array
     */
    static function get_list_film_high_view($number){
        $result=DB::query_list('SELECT * FROM films ORDER BY views DESC LIMIT  '.$number);
//        print_r($result);
        return $result;
    }
    
    static function create ($table,$data) {
        $result= DB::insert($table, $data);
    }
    
    function edit ($id, $data) {
        
    }
    
    static function delete ($id) {
        global $db;
        $db->query('DELETE FROM films WHERE id = ' . $id);
    }
    
    
    
    static function get_banners(){
        $query="SELECT * FROM panel";
        return DB::query_list($query);
                
    }
    /**
     * Get number of total product 
     */
    static function get_count_total() {
        $res = DB::get_record('SELECT COUNT(id) AS total FROM films');
        return $res['total'];
    }
    static function  get_list_limit($limit = 10, $offset = 0){
        return DB::query_list("SELECT p.* "
                            . "FROM films as p "
                            . "LIMIT $offset, $limit");
    }

}
