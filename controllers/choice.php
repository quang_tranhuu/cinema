<?php

//header('Content-Type: application/json');
require_once ('models/Category_model.php');
require_once ('models/Film_cinema_model.php');
require_once ('models/Cinema_model.php');
require_once ('models/Day_film_model.php');
require_once ('models/Day_model.php');
require_once ('models/Film_model.php');
require_once ('models/Schedule_model.php');
require_once ('models/Hour_name_model.php');



$action = isset($_GET['action']) ? $_GET['action'] : 'index';
switch ($action) {
    case 'list':
        include ('views/schedule.php');
    case 'list_cinema':
        $list_cinema = Cinema_model::get_list();
        include ('views/bkpage1.php');
        break;

    case 'list_films':
        $list_id_films = Film_cinema_model::get_list($_POST['id']);
        $list_films = array();
        foreach ($list_id_films as $film) {
            $list_films[] = Film_model::get_by_id($film['film_id']);
        }
        echo json_encode($list_films);
        break;

    case 'list_days':
        $list_id_days = Day_film_model::get_list_by_id($_POST['id']);
        $list_days = array();
        foreach ($list_id_days as $film) {
            $list_days[] = Day_model::get_by_id($film['day_id']);
        }
        echo json_encode($list_days);
        break;

    case 'list_times':
        $list_times = DB::get_col_where('schedules', array(
                    'day_id' => $_POST['id_day'],
                    'film_id' => $_POST['id_film'],
        ));
        $list = array();
        foreach ($list_times as $value) {
            $list[] = Hour_name_model::get_name_by_id($value['suat']);
        }
        echo json_encode($list);
        break;
    case 'create':
        $errors = [];
       
        if (!empty($_POST)) {
            $_SESSION['day_id']=$_POST['id_day'];
            $_SESSION['day_name']=$_POST['day_name'];
            $_SESSION['schedule_id']=$_POST['id_time'];
            $_SESSION['time_name']=$_POST['time_name'];
            $_SESSION['id_cinema']=$_POST['id_cinema'];
            $_SESSION['cinema_name']=$_POST['cinema_name'];
            $_SESSION['film_id']=$_POST['film_id'];
            $_SESSION['film_name']=$_POST['film_name'];
            $_SESSION['quantity_ticket']=$_POST['quantity_ticket'];
            $_SESSION['total_price']=$_POST['quantity_ticket']*PRICE;
            }
        $list_seats = DB::get_where('seats', array(
                    'day_id' => $_SESSION['day_id'],
                    'schedule_id' => $_SESSION['schedule_id'],
        ));
            
        include('views/bkpage2.php');
        break;
    case 'notification':

        echo 'thành công';
        include ('views/notification.php');
        break;
    case 'form_fill':
        $errors=[];
        if (isset($_POST) && count($_POST) > 0) {
            
            if (empty($_POST['username'])) {
                $errors['name'] = 'Required';
            }
            if (empty($_POST['tel'])) {
                $errors['tel'] = 'Required';
            }
            if (empty($_POST['id_card'])) {
                $errors['id_card'] = 'Required';
            }
            if (empty($_POST['email'])) {
                $errors['email'] = 'Required';
            }else {
                $okay = preg_match('/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $_POST['email']);
                if(!$okay){
                $errors['email'] = 'Invalid email format. Exp: abc@gmail.com';
                
                } 
            }
            if (!empty($_POST['seat_name']))
            $_SESSION['seat_name']=$_POST['seat_name'];
            if (!empty($_POST['id_seat']))
            $_SESSION['id_seat']=$_POST['id_seat'];
            
            if (empty($errors)) {
                if(!empty($_SESSION['username'])){
                    DB::insert('bookings', array(
                        'cinema_name' => $_SESSION['cinema_name'],
                        'film_name' => $_SESSION['film_name'],
                        'user_name' => $_POST['username'],
                        'id_user'=> $_SESSION['user_id'],
                        'quantity' => $_SESSION['quantity_ticket'],
                        'time' => $_SESSION['time_name'],
                        'date' => $_SESSION['day_name'],
                        'seat' => $_SESSION['seat_name'],
                        'tel' => $_POST['tel'],
                        'email' => $_POST['email'],
                        'id_card' => $_POST['id_card'],
                        'status'=> 'wait',
                    ));
                    $list_seatid=  explode(',', $_SESSION['id_seat']);
                    foreach ($list_seatid as $key => $value) {
                        DB::query("UPDATE seats SET stats='c' WHERE id=".$value);//xem lại viet ham moi ben model
                    } 
                    header("Location: " . DOMAIN . 'index.php?controller=film&action=list_booking');
                }else {
                        header("Location: " . DOMAIN . 'index.php?controller=auth&action=login');
                }
            }
        }
        
       
// After validate

        
        include ('views/bkpage3.php');
        break;
    case 'deleteses':
//        $_SESSION['film_id']=null;
//        $_SESSION['id_day']=null;
//        $_SESSION['schedule_id']=null;
        break;
    default:
        break;
}