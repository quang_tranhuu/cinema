
<!-- BEGIN html -->
<html lang = "en">
    <!-- BEGIN head -->
    <head>
        <!-- Meta Tags -->
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
        <title>Admin page</title>
        <meta name="description" content="" />
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
        <!-- Favicon -->
        <link rel="shortcut icon" href="<?php echo URL_IMAGE?>favicon4.ico" type="image/x-icon" />
        <!-- Stylesheets -->
        <link rel="stylesheet" href="<?php echo DOMAIN ?>public/css/bootstrap.min.css"/>
        <link rel="stylesheet" href="<?php echo DOMAIN ?>public/css/adminpage.css"/>
        <!--<script src="<?php echo DOMAIN ?>public/js/jquery-1.12.2.min.js"></script>-->
        <script src="<?php echo DOMAIN ?>public/js/bootstrap.min.js"></script>
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>reset.css" />
        <link type='text/css' rel='stylesheet' href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,700' />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>font-awesome.min.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>ot-menu.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>main-stylesheet.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>shortcodes.css" />
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>responsive.css" />
        <script src="<?php echo DOMAIN ?>public/js/jquery-1.12.2.min.js"></script>
        <!--[if lte IE 8]>
        <link type="text/css" rel="stylesheet" href="css/ie-ancient.css" />
        <![endif]-->
        <meta property="fb:app_id" content="{1032585593498383}" />

        <!-- Demo Only -->
        <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE ?>demo-settings.css" />

        <!-- END head -->
    </head>

    <!-- BEGIN body -->
    <body>
        <!-- BEGIN .boxed -->
        <div class="boxed">
            <!-- BEGIN .header -->
            <header class="header light">
                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <!-- BEGIN #main-menu -->
                    <nav id="main-menu" style="margin-top: 0px;">
                        <ul class="top-menu ot-menu-add" style="background-color: rgb(41, 128, 185);">
                            <li><a href="<?php echo DOMAIN ?>index.php">Home</a></li>
                            
                            <li class="welcomead">
                                <div class="top-right">
                                    Welcome <b><?php echo $_SESSION['username'] ?></b> | <a href="<?php echo DOMAIN . 'admin/index.php?controller=auth&action=logout' ?>" style="color: black">Logout</a>
                                </div>
                        </ul>
                        <!-- END #main-menu -->
                    </nav>
                    <!-- END .wrapper -->
                </div>
            </header>
            <!-- BEGIN .content -->
            <section class="content">
                <!-- BEGIN .wrapper -->
                <div class="wrapper" >
                    <div class="col-md-3 " style="padding: 0px;margin-bottom: 20px;">
                        <div class="profile-sidebar">
                                <!-- SIDEBAR USERPIC -->
                                <div class="profile-userpic">
                                    <img src="<?php echo URL_IMAGE.$_SESSION['image']?>" class="img-responsive" width="150" height="150" alt="">
                                </div>
                                <!-- END SIDEBAR USERPIC -->
                                <!-- SIDEBAR USER TITLE -->
                                <div class="profile-usertitle">
                                        <div class="profile-usertitle-name">
                                                <?php echo $_SESSION['name']?>
                                        </div>
                                </div>
                                <!-- END SIDEBAR USER TITLE -->
                                <!-- SIDEBAR BUTTONS -->

                                <!-- END SIDEBAR BUTTONS -->
                                <!-- SIDEBAR MENU -->
                                <div class="profile-usermenu">
                                        <ul class="nav">
                                                <li class="active">
                                                        <a href="#">
                                                        <i class="glyphicon glyphicon-home"></i>
                                                        Overview </a>
                                                </li>
                                                <li>
                                                        <a href="<?php echo DOMAIN_AD?>index.php?controller=user&action=edit">
                                                        <i class="glyphicon glyphicon-user"></i>
                                                        Change profile </a>
                                                </li>
                                                <li>
                                                        <a href="<?php echo DOMAIN_AD?>index.php?controller=booking&action=list_booking" >
                                                        <i class="glyphicon glyphicon-film"></i>
                                                        Booking wait</a>
                                                </li>
                                                <li>
                                                        <a href="<?php echo DOMAIN_AD?>index.php?controller=booking&action=checked">
                                                        <i class="glyphicon glyphicon-ok "></i>
                                                        Booking checked </a>
                                                </li>
                                                <li>
                                                        <a href="<?php echo DOMAIN_AD?>index.php?controller=product&action=manage_film">
                                                        <i class="glyphicon glyphicon-film"></i>
                                                        Manage films </a>
                                                </li>
                                                <li>
                                                        <a href="<?php echo DOMAIN_AD?>index.php?controller=user&action=list_user">
                                                        <i class="glyphicon glyphicon-user"></i>
                                                        Manage member </a>
                                                </li>
                                                
                                        </ul>
                                </div>
                                <!-- END MENU -->
                        </div>
                    </div>