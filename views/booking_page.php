<?php include 'views/layout/header.php'; ?>
<div class="container">

    <form action="<?php echo DOMAIN;?>index.php?controller=choice&action=form_fill" method="POST" id="choseseat">
        <input type="hidden" name="cinema_name" value="<?php echo $_POST['cinema_name']; ?>">
        <input type="hidden" name="film_name" value="<?php echo $_POST['film_name']; ?>">
        <input type="hidden" name="day_name" value="<?php echo $_POST['day_name']; ?>">
        <input type="hidden" name="time_name" value="<?php echo $_POST['time_name']; ?>">
        <input type="hidden" id="number_ticket" name="num" value="<?php echo $_POST['number_ticket']; ?>">
        <input id="seat_name" type="hidden" name="seat_name" value="">
        <input id="id_seat" type="hidden" name="id_seat" value="">
        <div class="row">
            <p>Cinema : <?php echo $_POST['cinema_name']; ?>
            <p>Film :<?php echo $_POST['film_name']; ?>
            <p>Day : <?php echo $_POST['day_name']; ?>
            <p>Time :<?php echo $_POST['time_name']; ?>
            <p>Quantity: <?php echo $_POST['number_ticket']; ?>
        </div>
        <div class="row">
            <div class="row col-md-6 ghe">
                <?php foreach ($list_seats as $value) { ?>
                    <input type="button" onclick="selectSeat(<?php echo $value['id']; ?>)" id="<?php echo $value['id']; ?>" name="id_seat" class="btn <?php echo $value['stats'] ? 'btn-danger' : 'btn-default' ?>" 
                           placeholder="<?php echo $value['stats'] ?>" value="<?php echo $value['name'] ?>">
                       <?php } ?>
            </div>
        </div>
        <div class="row"  >
            <button type="button" id="ok" onclick="Continue()"  >OK</button>
            <input type="submit" value="Tiếp tục" style="display: none" id="tt">
        </div>
    </form>
<!--    <div class="container" id="confirm" style="display: none">-->
<!--<form method="POST" action="<?php echo DOMAIN;?>index.php?controller=choice&action=form_fill" style="display: none" id="tt1">        
<div class="row">
    <input type="hidden" name="cinema_name" value="<?php echo $_POST['cinema_name']; ?>">
    <input type="hidden" name="film_name" value="<?php echo $_POST['film_name']; ?>">
    <input type="hidden" name="day_name" value="<?php echo $_POST['day_name']; ?>">
    <input type="hidden" name="time_name" value="<?php echo $_POST['time_name']; ?>">
    <input type="hidden" id="number_ticket" name="num" value="<?php echo $_POST['number_ticket']; ?>">
    <input id="seat_name" type="hidden" name="seat_name" value="">
    <input id="id_seat" type="hidden" name="id_seat" value="">
    <input type="submit" value="Tiếp tục">
        </div>
</form>-->

</div>
<script>
    var numberSeat = $("#number_ticket").attr('value');
    var seatCount = 0;
    var _listSeatName = "";
    var _listSeatID="";
//    $("form#choseseat").submit(function (e) {
//        e.preventDefault();
//    });
//    alert(numberSeat);
    function selectSeat(seatID) {
        // xử lý check chọn ghế
//            var cla=$("#"+seatID).attr('class');
//            alert(cla);
        var _value = $("#" + seatID).attr('class');
        if (_value == "btn btn-default") {
            $("#" + seatID).attr('class', 'btn btn-primary');
            seatCount += 1;
        }
        else if (_value == "btn btn-primary") {
            $("#" + seatID).attr('class', 'btn btn-default');
            seatCount -= 1;
        }
        else if (_value == "btn btn-danger") {
            alert('ghế đã có người đặt');
        }          

        if (seatCount > numberSeat) {
            var _classSeat = $("#" + seatID).attr('class');
            if (_classSeat == "btn btn-primary") {
                $("#" + seatID).attr('class', 'btn btn-default');
                seatCount -= 1;
            }
            alert('You only select ' + parseFloat(numberSeat) + ' Seat');
            return false;
        }

    };
    function GetListSeat() {
        var a = $("input");
        
        a.each(function () {
            if ($(this).attr('class') == "btn btn-primary") {
                _listSeatName += $(this).attr('value') + ",";
                _listSeatID += $(this).attr('id') + ",";
//                   alert(_listSeat);

            }
        });
    };
//    $()
    function Continue() {
        
        if (seatCount < numberSeat) {
            alert('Bạn phải chọn thêm' + (numberSeat - seatCount) + 'ghế !');
            return false;
        }
        GetListSeat();        
        if (_listSeatName != "" && _listSeatID!="") {
            $("#seat_name ").attr('value', _listSeatName);
            $("#id_seat ").attr('value', _listSeatID);
            $("button#ok").hide();
            $("input#tt").show();
//            $("#confirm").show();
        }
        else {
            alert('Please select seat');
            return false;
        }

    };
    
</script>
<?php include 'views/layout/footer.php'; ?>