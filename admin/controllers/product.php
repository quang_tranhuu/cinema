<?php

require_once ('../models/Film_model.php');
require_once ('../models/Category_model.php');
require_once ('../models/Cinema_model.php');
require_once ('../models/Film_cinema_model.php');
require_once ('../models/Cinema_model.php');
require_once ('../models/Day_film_model.php');
require_once ('../models/Day_model.php');
require_once ('../models/Schedule_model.php');
require_once ('../models/Hour_name_model.php');
require_once ('models/mail_model.php');

switch ($action) {
    case 'manage_film':
        require_once '../library/Pagination.php';
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        // Get number of total product record in DB
        $total_record = Film_model::get_count_total();
        $limit = 5;
        // Get list product records by page
        $offset = ($page - 1) * $limit;
        $list_films = Film_model::get_list_limit($limit, $offset);
        include('views/product/manage_film.php');
        break;
    /////
    case 'create':

        $errors =[];

        if (isset($_POST) && count($_POST) > 0) {
            // Validation
            if ($_POST['name'] == '') {
                $errors['name'] = 'Required';
            }
            if ($_POST['typefilm'] == '') {
                $errors['typefilm'] = 'Required';
            } 
            if($_POST['actor']==''){
                $errors['actor'] = 'Required';
            }
            if($_POST['director']==''){
                $errors['director'] = 'Required';
            }
            if($_POST['duration']==''){
                $errors['duration'] = 'Required';
            }
            if($_POST['starttime']==''){
                $errors['starttime'] = 'Required';
            }
            if($_POST['trailer']==''){
                $errors['trailer'] = 'Required';
            }
            if($_POST['introduce']==''){
                $errors['introduce'] = 'Required';
            }
            if($_FILES['image']['name']==''){
                $errors['image'] = 'Required';
            }
            // After validate
            print_r($errors);      
            if (empty($errors)) {
                $extend=DB::checkTypeImage($_FILES['image']['type']);
                $pathnew=PATH.$_FILES['image']['name'];
                if(file_exists($pathnew)){              //check file exist
                    unlink($pathnew);
                }
                $tmp_name = $_FILES['image']['tmp_name'];
                $success = move_uploaded_file($tmp_name, $pathnew);
                DB::insert('films',array(
                    'name' => $_POST['name'],
                    'typefilm' => $_POST['typefilm'],
                    'actor' => $_POST['actor'],
                    'director' => $_POST['director'],
                    'duration' => $_POST['duration'],
                    'starttime' => $_POST['starttime'],
                    'trailer' => $_POST['trailer'],
                    'introduce' => $_POST['introduce'],
                    'image' => $_FILES['image']['name'],
                ));
            }
        }

        include('views/product/create_film.php');
        break;
      
    case 'delete':
        DB::query('DELETE FROM films WHERE id='.$_GET['id']);
        header("Location: " . DOMAIN . 'admin/index.php?controller=product&action=index');
        break;
    
    
    case 'edit' :
        $errors =[];
        $film= DB::get('films', $_GET['id']);
//        print_r($film);
        if (isset($_POST) && count($_POST) > 0) {
            // Validation
            if ($_POST['name'] == '') {
                $errors['name'] = 'Required';
            }
            if ($_POST['typefilm'] == '') {
                $errors['typefilm'] = 'Required';
            } 
            if($_POST['actor']==''){
                $errors['actor'] = 'Required';
            }
            if($_POST['director']==''){
                $errors['director'] = 'Required';
            }
            if($_POST['duration']==''){
                $errors['duration'] = 'Required';
            }
            if($_POST['starttime']==''){
                $errors['starttime'] = 'Required';
            }
            if($_POST['trailer']==''){
                $errors['trailer'] = 'Required';
            }
            if($_POST['introduce']==''){
                $errors['introduce'] = 'Required';
            }
            if($_FILES['image']['name']==''){
                $errors['image'] = 'Required';
            }
            // After validate
            if (empty($errors)) {
                if(file_exists(PATH.$_FILES['image']['name'])){
                    unlink(PATH.$_FILES['image']['name']);
                }
                $tmp_name = $_FILES['image']['tmp_name'];
                $name = PATH.$_FILES['image']['name'];
                $success = move_uploaded_file($tmp_name, $name);
                DB::update('films',array(
                    'name' => $_POST['name'],
                    'typefilm' => $_POST['typefilm'],
                    'actor' => $_POST['actor'],
                    'director' => $_POST['director'],
                    'duration' => $_POST['duration'],
                    'starttime' => $_POST['starttime'],
                    'trailer' => $_POST['trailer'],
                    'introduce' => $_POST['introduce'],
                    'image' => $_FILES['image']['name'],
                    
                ),$_GET['id']);
                $message="Update success !";
                
            }
        }

        include('views/product/edit_film.php');
        break;
    case 'change_schedule':
        $film=DB::get('films', $_GET['id']);
        $list_cinema=  Cinema_model::get_list();
        $list_day= Day_model::get_list();
        $list_schedule= Hour_name_model::get_list();
        if (isset($_POST) && count($_POST) > 0) {
            // Validation

            if (DB::get_post('cinema', '')== '') {
                $errors['cinema'] = 'Required';
//                print_r($errors);
//                exit;
            } 
            if(DB::get_post('day', '')==''){
                $errors['day'] = 'Required';
            }
            if(DB::get_post('schedule', '')==''){
                $errors['schedule'] = 'Required';
            }
//            var_dump($_POST['cinema']);
//            var_dump($errors);
//            var_dump($_POST['schedule']);
//            exit;
            if (empty($errors)) {
                foreach ($_POST['cinema'] as $value) {
//                    print_r($value);
                    $item= DB::get_where('film_cinemas', array(
                                                         'cinema_id'=>$value,
                                                         'film_id'=> $_GET['id'],
                                                         ));
                    if(empty($item)){
                       DB::insert('film_cinemas', array(
                                                        'film_id'=>$_GET['id'],
                                                        'cinema_id'=>$value,)) ;
                    }
                }
                ///end insert film into cinema
                //begin insert day to film,schedule to day
                foreach ($_POST['day'] as $day) {
//                    print_r($day);
                    $item_day_film= DB::get_where('day_film', array(
                                                         'day_id'=>$day,
                                                         'film_id'=> $_GET['id'],
                                                         ));
                    foreach ($_POST['schedule'] as $sche) {
                        $item_schedule_day= DB::get_where('schedules', array(
                                                         'day_id'=>$day,
                                                         'film_id'=> $_GET['id'],
                                                         'suat'=> $sche,
                                                         ));
                        //insert film_id,day_id,suat into schedules
                        if(empty($item_schedule_day)){
                            DB::insert('schedules', array(
                                                        'film_id'=>$_GET['id'],
                                                        'day_id'=>$day,
                                                        'suat'=> $sche,
                                )) ;
                        }
                    }
                    
                    //insert film_id,day_id into day_film
                    if(empty($item_day_film)){
                       DB::insert('day_film', array(
                                                        'film_id'=>$_GET['id'],
                                                        'day_id'=>$day,)) ;
                    }
                }
                
            }
        }  else {
            $message='Bạn chưa chọn mục nào. Xin chọn đầy đủ các mục !';
        }
        include ('views/product/change_schedule.php');
        break;
    
    
    ///////
    case 'upload':
        include ('views/product/upload.php');
        print_r($_FILES);
        $tmp_name = $_FILES['image']['tmp_name'];
        $path = getcwd() . DIRECTORY_SEPARATOR . 'images';
        echo $path;
        $name = $path . DIRECTORY_SEPARATOR . $_FILES['image']['name'];
        $success = move_uploaded_file($tmp_name, $name);
        if ($success) {
            echo $mess = $name . 'has upload';
        }
        break;
    case 'send_mail':
        break;
    case 'test_mail':
        require_once '../library/PEAR_Mail/Mail.php';
        $options = array();
        $options['host'] = 'ssl://smtp.gmail.com';
        $options['port'] = 465;
        $options['auth'] = TRUE;
        $options['username'] = 'huuquangbk151';
        $options['password'] = 'tranhuudue';
        $mail = Mail::factory('smtp', $options);
        $headers = array(
            'From' => 'huuquangbk151@gmail.com',
            'To' => 'huuquangbk151@gmail.com',
            'Subject' => 'test email',
            'Content-type' => 'text/html'
        );
        $reciptions = 'huuquangbk151@gmail.com';

        $mesage = '<p>text</p>' . '<p style="color:red">ABC</p>';

        $result = $mail->send($reciptions, $headers, $mesage);
        if (PEAR::isError($result)) {
            echo $result->getMessage();
        }
        break;
    case 'curl':
        $ch = curl_init('http://stc-tv.zing.vn/skins/tv_v2/camp_oppotet/images_camp/camp_oppotet/h11.jpg');
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);
        $data = curl_exec($ch);
        curl_close($ch);
        $fp = fopen('anh2.jpg', 'x');
        fwrite($fp, $data);
        fclose($fp);
        break;
    case 'curl_post':
        $ch=  curl_init('http://nguoiphattu.com/search.html');
        curl_setopt($ch, CURLOPT_POSTFIELDS, "keyword=phat");
        $data = curl_exec($ch);
        curl_close($ch);
}

