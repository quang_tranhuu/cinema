
<?php include 'views/layout/header_personpage.php' ?>
<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content" id="createb" >
        <form method="POST" action="" enctype="multipart/form-data">
        <div class="content-panel" >
                <div class="panel-title">
                    <h2>Edit Profile </h2>
                </div>
                <div class="panel-block" style="width: 100%;margin: auto">
                    <div class="form-group">
                    <label class="col-md-3 control-label " for="name">Name</label>  
                    <div class="col-md-6">
                        <input id="name" name="name" value="<?php echo DB::get_post('name', $user[0]['name'])?>" type="text" placeholder="" class="form-control input-md" >
                    </div>
                    <div class="col-md-3" style="color: red">
                        <span>*<?php echo isset($errors['name'])? $errors['name']: '';?></span>
                    </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="pass" >Password</label>  
                        <div class="col-md-6">
                            <input  id="pass" name="password" value="<?php echo DB::get_post('password', '')?>"  type="password" placeholder="" class="form-control input-md">

                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['password'])? $errors['password']: '';?></span>
                        </div>
                    </div>
                     <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="new" >Password New </label>  
                        <div class="col-md-6">
                            <input  id="new" name="passwordnew" value="<?php echo DB::get_post('passwordnew', '')?>"  type="password" placeholder="" class="form-control input-md">

                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['passwordnew'])? $errors['passwordnew']: '';?></span>
                        </div>
                    </div>
                     <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="confirm" >Password Confirm</label>  
                        <div class="col-md-6">
                            <input  id="confirm" name="repassword" value="<?php echo DB::get_post('repassword','')?>"  type="password" placeholder="" class="form-control input-md">

                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['repassword'])? $errors['repassword']: '';?></span>
                        </div>
                    </div>
                     <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="tel">Tel</label>  
                        <div class="col-md-6">
                            <input id="tel" name="tel" value="<?php echo DB::get_post('tel', $user[0]['tel'])?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['tel'])? $errors['tel'] : '';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="address">Address</label>  
                        <div class="col-md-6">
                            <input id="address" name="address" value="<?php echo DB::get_post('address', $user[0]['address'])?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['address'])? $errors['address'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">Email</label>  
                        <div class="col-md-6">
                            <input id="email" name="email" value="<?php echo DB::get_post('email', $user[0]['email'])?>"  type="email" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['email'])? $errors['email'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="image">Image</label>  
                        <div class="col-md-6">
                            <input  id="image" name="image" value="<?php echo isset($_FILES['image']['name']) ? $_FILES['image']['name'] : ''?>"  type="file" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['image'])? $errors['image'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4">
                            <button id="" type="submit" class="btn btn-primary" >Submit</button>
                        </div>
                    </div>
                <div style="color: #28a4c9"> <?php echo isset($message)? $message:''?></div>
                </div>
                
            </div>
        </form>
    </div>
</div>

<?php include 'views/layout/footer_personpage.php' ?>