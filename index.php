<?php
session_start();
include 'config.php';
require_once 'library/DB.php';
DB::connect();

$controller = isset($_GET['controller']) ? $_GET['controller'] : 'homepage';
$action = isset($_GET['action']) ? $_GET['action'] : 'index';

include('controllers/' . $controller .'.php');

