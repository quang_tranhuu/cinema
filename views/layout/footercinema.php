<!-- BEGIN .footer -->
            <footer class="footer">
                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <h3>Popular Videos</h3>
                        <div class="widget-videos-small">
                            <!-- BEGIN .item -->
                            <div class="item">
                                <div class="item-header">
                                    <a href="post.html" class="video-thumb "><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://i.vimeocdn.com/video/435803636_295x166.jpg" alt="" /></a>
                                </div>
                                <div class="item-content">
                                    <a href="browse.html"><span class="marker">HD Videos</span></a>
                                    <h3><a href="post.html">Nuit Blanche</a></h3>
                                    <span class="video-meta">
                                        <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                        <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                        <a href="#"><i class="fa fa-heart"></i>95</a>
                                    </span>
                                </div>
                            <!-- END .item -->
                            </div>
                            <!-- BEGIN .item -->
                            <div class="item">
                                <div class="item-header">
                                    <a href="post.html" class="video-thumb "><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/500/390/50039066_295.jpg" alt="" /></a>
                                </div>
                                <div class="item-content">
                                    <a href="post.html"><span class="marker">Creative Stuff</span></a>
                                    <h3><a href="post.html">Nokta.</a></h3>
                                    <span class="video-meta">
                                        <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                        <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                        <a href="#"><i class="fa fa-heart"></i>95</a>
                                    </span>
                                </div>
                            <!-- END .item -->
                                </div>
                            <!-- BEGIN .item -->
                            <div class="item">
                                <div class="item-header">
                                    <a href="post.html" class="video-thumb "><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/244/196/244196772_295.jpg" alt="" /></a>
                                </div>
                                <div class="item-content">
                                    <a href="post.html"><span class="marker">Experimental</span></a>
                                    <h3><a href="post.html">PROTEIGON</a></h3>
                                    <span class="video-meta">
                                        <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                        <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                        <a href="#"><i class="fa fa-heart"></i>95</a>
                                    </span>
                                </div>
                            <!-- END .item -->
                            </div>
                        </div>
                    <!-- END .widget -->
                    </div>
                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <h3>Tag Cloud</h3>
                        <div class="tagcloud">
                            <a href="browse.html">Creative</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Temporibus reformid</a>
                            <a href="browse.html">Democ</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Creative</a>
                            <a href="browse.html">Creative</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Temporibus reformid</a>
                            <a href="browse.html">Democ</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Creative</a>
                        </div>
                    <!-- END .widget -->
                    </div>
                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <h3>Contact Us</h3>
                        <div class="htmlcode">
                            <p>HDcinema.com is a website to booking film ticket.Let's enjoy your's life with HDcinema.com</p>
                            <ul>
                                <li><span class="small-text">Address</span><h6>122 Tran Phu Street, Hai Chau<br/>Da Nang, Viet Nam</h6></li>
                                <li><span class="small-text">Phone number</span><h6>0511 241 3300</h6></li>
                                <li><span class="small-text">E-mail address</span><h6>support@gmail.com</h6></li>
                            </ul>
                        </div>
                    <!-- END .widget -->
                    </div>
                <!-- END .wrapper -->
                </div>
            <!-- END .footer -->
            </footer>
            <div class="footer-bottom">
                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <ul class="right">
                        <li><a href="index-2.html">Home</a></li>
                        <li><a href="browse.html">Browse</a></li>
                        <li><a href="browse.html">Popular</a></li>
                        <li><a href="sample.html">Sample Page</a></li>
                        <li><a href="contact-us.html">Contact Us</a></li>
                    </ul>
                    <p>&copy; 2014 Copyright <b>HDcinema</b>. All Rights reserved.</p>
                <!-- END .wrapper -->
                </div>
            </div>
        <!-- END .boxed -->
        </div>
        <script>
                // Video set layout
		$("a[href='#v-set-layout']").click(function(){
			var element = $(this);
			element.addClass("active").siblings(".active").removeClass("active");
			element.parent().parent().siblings(".panel-block").attr("class", "panel-block video-list").addClass(element.attr("rel"));
			return false;
		});
                
        </script>
            <!--<script type="text/javascript" src="<?php echo URL_JS?>navbar.js"></script>-->
        <!-- Scripts -->
        <!--<script type="text/javascript" src="<?php echo URL_JS?>jquery-latest.min.js"></script>-->
        <script type="text/javascript" src="<?php echo URL_JS?>ot-menu.js"></script>
        <!--<script type="text/javascript" src="<?php echo URL_JS?>theme-scripts.js"></script>-->
        <!--<script type="text/javascript" src="<?php echo URL_JS?>jwplayer.js"></script>-->
        <!--<script type="text/javascript">jwplayer.key="Dtw8XIrirt0jOoDeYv+GewD2piVCeaDQezuMKg==";</script>-->
        <!-- Demo Only -->
        <!--<script type="text/javascript" src="<?php echo URL_JS?>demo-settings.js"></script>-->
    </body>
</html>