<?php

class User_model1 {
    
    static function get_user($username = '') 
    {
        return DB::get_where('users', [
            'username' => $username
        ]);
    }
    static function get_user_by_id($iduser) 
    {
        return DB::get_where('users', [
            'id' => $iduser
        ]);
    }
    static function get_all_user(){
        return DB::query_list('SELECT * FROM users');
    }
}