<?php include 'views/layout/header_admin.php' ?>
<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content">
        <div class="row">
        <div class="col-md-8">
        <h1> BOOKING CHECKED</h1>
    </div>
    
    <div class="col-md-12">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>STT</th> 
                    <th>Người đặt</th> 
                    <th>Phim</th> 
                    <th>Suất</th> 
                    <th>Ngày</th> 
                    <th>Ghế</th>
                    <th>Rạp</th>
                    <!--<th>Action</th>-->
                </tr> 
            </thead> 
            <tbody> 
                <?php foreach($list_booking as  $item) { ?>
                <tr> 
                    <th scope="row"><?php echo ++$offset; ?></th> 
                    <td><?php echo $item['user_name'] ?></td>
                    <td><?php echo $item['film_name'] ?></td>
                    <td><?php echo $item['time'] ?></td>
                    <td><?php echo $item['date'] ?></td>
                    <td><?php echo $item['seat'] ?></td>
                    <td><?php echo $item['cinema_name'] ?></td>
<!--                    <td>
                        <a href="<php echo DOMAIN_AD .'index.php?controller=booking&action=reject&id=' . $item['id'] ?>"><i class="fa fa-trash-o "></i></a> &nbsp;
                        <a href="<php echo DOMAIN_AD .'index.php?controller=booking&action=accept&id=' . $item['id'] ?>"><i class="fa fa-check-square-o"></i></a> 
                    </td>-->
                </tr> 
                <?php } ?>
            </tbody> 
        </table>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
        <nav>
            <?php echo Pagination::render($total_record, $limit, $page, DOMAIN . 'admin/index.php?controller=booking&action=checked&page='); ?>
        </nav>
        </div>
        
    </div> 
        </div>
    </div>
</div>
<?php include 'views/layout/footer_admin.php' ?>