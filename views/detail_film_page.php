<?php include 'views/layout/header1.php'; ?>
<!-- BEGIN .video-slider -->
<div class="video-slider">
    <!-- BEGIN .wrapper -->
    <div class="wrapper">
        <div class="slider-controls">
            <div class="video-embed">

                <div class="otplayer-wrapper">
                    <iframe width="560" height="315" src="<?php echo $result[0]['trailer']?>" frameborder="0" allowfullscreen></iframe>
                    
                </div>

            </div>
        </div>
        <div class="video-slider-meta">
<!--            <div class="video-slider-info right">
                <a href="post.html" class="meta-click"><i class="fa fa-comments"></i> <strong>283</strong> comments</a>
                <a href="post.html" class="meta-click"><i class="fa fa-eye"></i> <strong>829</strong> views</a>
                <a href="#" class="ot-like-button"><i class="fa fa-heart"></i> Like This Video</a>
            </div>-->
            <h3><?php echo $result[0]['name']?><span>&nbsp;Trailer</span></h3>
        </div>
    <!-- END .wrapper -->
    </div>
<!-- END .video-slider -->
</div>
<!-- END .header -->
</head>
<!-- BEGIN .content -->
    <section class="content">
        <!-- BEGIN .wrapper -->
        <div class="wrapper">
                <!-- BEGIN .with-sidebar-layout -->
                <div class="with-sidebar-layout left">
                    <div class="content-panel">
                        <div class="panel-title">
                            <h2>Film Details</h2>
                        </div>
                        <div class="panel-block">

                            <div class="panel-content">
                                <div class="video-author">
                                    <img src="<?php echo URL_IMAGE?>photos/image-1.jpg" class="left" alt="" />
                                    <div class="author-content">
                                        <span>Added by <a href="#">Admin</a></span>
                                        <span>March 26, 2014</span>
                                    </div>
                                    <div class="clear-float"></div>
                                </div>

                                <p><?php echo $result[0]['introduce']?></p>

                                <div class="video-footer">
                                    <strong>Category</strong>
                                    <p>
                                        <a href="#">Tacimates</a>
                                        <a href="#">Expetenda</a>
                                        <a href="#">Praesent</a>
                                        <a href="#">Electram</a>
                                        <a href="#">Maiorum</a>
                                        <a href="#">Mandamus</a>
                                    </p>
                                    <strong>Tags</strong>
                                    <p>
                                        <a href="#">Fierent</a>
                                        <a href="#">Mediocrem</a>
                                        <a href="#">Pertinax</a>
                                        <a href="#">Expetendis</a>
                                        <a href="#">Constituam</a>
                                    </p>
                                </div>
                            </div>

                        </div>
                    </div>
<!--                    <div class="content-panel">
                        <div class="panel-title">
                            <h2>Similar Videos</h2>
                            <div class="right video-set-layout">
                                <a href="#v-set-layout" rel="grid" class="active"><i class="fa fa-th"></i></a>
                                <a href="#v-set-layout" rel="hdgrid"><i class="fa fa-th-large"></i></a>
                                <a href="#v-set-layout" rel="list"><i class="fa fa-th-list"></i></a>
                                <a href="#v-set-layout" rel="hdlist"><i class="fa fa-bars"></i></a>
                            </div>
                        </div>
                        <div class="panel-block video-list grid">
                                 BEGIN .item 
                            <div class="item">
                                <div class="item-header">
                                    <a href="post.html" class="img-hover-effect loadingvideo"><img src="images/aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/629/174/62917463_640.jpg" alt="" /></a>
                                </div>
                                <div class="item-content">
                                    <h3><a href="post.html">Hair Conditioner</a></h3>
                                    <span class="video-meta">
                                        <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                        <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                        <a href="#"><i class="fa fa-heart"></i>95</a>
                                    </span>
                                    <p>Inani vocent feugait cu eam, his et impetus indoctum, mea euismod salutandi. Mel consequat moderatius intellegeb at an, appareat pertinacia no pro, noster aperiam blandit vim. Ne mei illud quidam labitur, eu adhuc clita quo.</p>
                                </div>
                             END .item 
                            </div>
                        </div>
                    </div>-->
                    <div class="content-panel">
                        <div class="panel-title">
                            <h2> Comments</h2>
                        </div>
                        <div class="panel-block">
                            <!-- BEGIN #comments -->
                            <!--<div class="fb-comments" data-href="<?php echo DOMAIN?>index.php?controller=film&amp;action=detail&amp;id=<?php $result[0]['id']?>" data-numposts="10"></div>-->
                            <div class="fb-comments" data-href="http://localhost/cinema/index.php?controller=film&amp;action=detail&amp;id=<?php $result[0]['id']?>" data-numposts="10"></div>
                            <!--<div class="fb-comments" data-href="http://hdcinema.com/cinema/index.php?controller=film&amp;action=detail&amp;id=<?php $result[0]['id']?>" data-numposts="10"></div>-->
                            <div
                              class="fb-like"
                              data-share="true"
                              data-width="450"
                              data-show-faces="true">
                            </div>

                        </div>
                    </div>
                <!-- END .with-sidebar-layout -->
                </div>

                <!-- BEGIN #sidebar -->
                <aside id="sidebar" class="right">
                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <h3>Video Statistics</h3>
                        <div class="video-stats">
                            <div class="video-stat-blobs">
                                <span>
                                    <strong><?php echo $result[0]['views']?></strong>
                                    <i>views</i>
                                </span>
                                <span>
                                    <strong>392</strong>
                                    <i>comments</i>
                                </span>
                                <span>
                                    <strong>201</strong>
                                    <i>likes</i>
                                </span>
                            </div>
<!--                            <div class="hr-spacer"></div>
                            <h5>About Orange-Themes</h5>
                            <p>Eos alii duis comprehensam ea. Ad vix sumo tim eam petentium, soluta corpora mnesarchum ex nemore everti dolorem at, eu mazim.</p>-->
                        </div>
                    <!-- END .widget -->
                    </div>

                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <div class="banner-widget no-border">
                            <a href="#" target="_blank"><img src="<?php echo URL_IMAGE?>no-banner-300x250.jpg" width="300" height="250" alt="" /></a>
                            <a href="contact-us.html" class="banner-meta"><i class="fa fa-angle-double-up"></i> Contact us about advert spaces <i class="fa fa-angle-double-up"></i></a>
                        </div>
                    <!-- END .widget -->
                    </div>

                        <!-- BEGIN .widget -->
<!--                        <div class="widget">
                            <h3>Featured Videos</h3>
                            <div class="widget-videos">
                                 BEGIN .item 
                                <div class="item">
                                    <div class="item-header">
                                        <a href="post.html" class="video-thumb loadingvideo"><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/160/609/160609628_640.jpg" alt="" /></a>
                                    </div>
                                    <div class="item-content">
                                        <h3><a href="post.html">Manhattan in motion</a><a href="#"><span class="marker">HD Videos</span></a></h3>
                                        <span class="video-meta">
                                            <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                            <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                            <a href="#"><i class="fa fa-heart"></i>95</a>
                                        </span>
                                    </div>
                                 END .item 
                                </div>
                            </div>
                         END .widget 
                        </div>-->

                        <!-- BEGIN .widget -->
<!--                        <div class="widget">
                            <h3>Popular Videos</h3>
                            <div class="widget-videos-small">
                                 BEGIN .item 
                                <div class="item">
                                    <div class="item-header">
                                        <a href="post.html" class="video-thumb loadingvideo"><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/580/274/58027488_295.jpg" alt="" /></a>
                                    </div>
                                    <div class="item-content">
                                        <a href="post.html"><span class="marker">Creative Stuff</span></a>
                                        <h3><a href="post.html">Television is a drug.</a></h3>
                                        <span class="video-meta">
                                            <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                            <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                            <a href="#"><i class="fa fa-heart"></i>95</a>
                                        </span>
                                    </div>
                                 END .item 
                                    </div>
                            </div>
                         END .widget 
                        </div>-->
                <!-- END #sidebar -->
                </aside>
                <div id="side-left" >
                            
                <a href="#" target="_blank">                    
                    <img src="<?php echo URL_IMAGE?>ad1.jpg" width="150" height="800">    
                </a>
                </div>
                <div id="side-right" > 
                            
                <a href="#" target="_blank">                    
                    <img src="<?php echo URL_IMAGE?>ad2.jpg" width="150" height="800">    
                </a>
            	</div>
        <!-- END .wrapper -->
        </div>
    <!-- BEGIN .content -->
    </section>
<?php include 'views/layout/footercinema.php'; ?>

