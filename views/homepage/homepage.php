
<!-- BEGIN html -->
<html lang = "en">
    <!-- BEGIN head -->
    <head>
            <!-- Meta Tags -->
            <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
            <title>HDcinema | Homepage </title>
            <meta name="description" content="" />
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

            <!-- Favicon -->
            <link rel="shortcut icon" href="<?php echo URL_IMAGE?>favicon4.ico" type="image/x-icon" />

            <!-- Stylesheets -->
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>reset.css" />
            <link type='text/css' rel='stylesheet' href='http://fonts.googleapis.com/css?family=Titillium+Web:400,600,700' />
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>font-awesome.min.css" />
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>ot-menu.css" />
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>main-stylesheet.css" />
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>shortcodes.css" />
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>responsive.css" />
            <script src="<?php echo DOMAIN ?>public/js/jquery-1.12.2.min.js"></script>
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>bootstrap.min.css" />
            <!-- Demo Only -->
            <link type="text/css" rel="stylesheet" href="<?php echo URL_STYLE?>demo-settings.css" />
            <script type="text/javascript" src="<?php echo URL_JS?>popup.js"> </script>
            <link rel="stylesheet" href="<?php echo URL_STYLE?>popup.css"/>
    <!-- END head -->
    </head>

	<!-- BEGIN body -->
        <body style="background-color: #efefef;">
            <div id="boxes">
            <div style="top: 199.5px; left: 551.5px; display: none;" id="dialog" class="window">
                <img alt="ads" src="<?php echo URL_IMAGE?>popup.jpg" > 
                <a href="#" class="close fa fa-times-circle fa-2x" style="color: black" ></a>
            </div>
            <!-- Mask to cover the whole screen -->
            <div style="width: 1478px; height: 602px; display: none; opacity: 0.8;" id="mask"></div>
            </div>
		<!-- BEGIN .boxed -->
        <div class="boxed">
                <!-- BEGIN .header -->
            <header class="header light">


                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <!-- BEGIN .header-content -->
                    <div class="header-content" style="margin-top: 0px;">

                        <div class="head" style="padding: 5px 0;">
                            <img src="<?php echo URL_IMAGE?>head.png" width="100%" height="150">
                        </div>

                        <!-- END .header-content -->
                    </div>
                    <!-- BEGIN #main-menu -->
                    <nav id="main-menu">
                        <ul class="top-menu ot-menu-add" style="background-color: rgb(41, 128, 185);">
                            <li><a href="<?php echo DOMAIN ?>index.php?controller=choice&action=list_cinema">Lịch chiếu</a></li>
                            <li><a href=""><span>Phim</span></a>
                                <ul>
                                    <li><a href="">Phim đang chiếu</a></li>
                                    <li><a href="">Phim sắp chiếu</a></li>
                                </ul>
                            </li>
                            <li>
                                <a href="">Rạp phim</a>
                            </li>
                            <li><a href="">Khuyến mãi</a></li>
                            <li><a href="contact-us.html">Liên hệ</a></li>
                            <li class="welcome" style="display: <?php echo DB::check_session('username', 'none')?>">
                                <a href="<?php echo DOMAIN?>index.php?controller=auth&action=login">Login</a></li>
                            <li class="welcome" style="display: <?php echo DB::check_session('username', 'none')?>">
                                <a href="<?php echo DOMAIN?>index.php?controller=auth&action=register">Register</a></li>
                            <li class="ad" style="display: <?php echo isset($_SESSION['username'])? '' :'none';?>" >
                                <a href=""><?php echo isset($_SESSION['username'])? $_SESSION['username']:'' ?></a>
                                <ul>
                                    <li><a href="<?php echo DOMAIN . 'index.php?controller=personpage' ?>">Profile</a></li>
                                    <li><a href="<?php echo DOMAIN . 'index.php?controller=auth&action=logout' ?>">Logout</a></li>
                                </ul>
                            </li>
                            <li class="ad" style="display: <?php echo isset($_SESSION['username'])? '' :'none';?>" >
                                <img src="<?php echo URL_IMAGE.$_SESSION['image']?>" class="img-circle" width="40" height="40" style="float: right;">

                            </li>
                        </ul>
                    <!-- END #main-menu -->
                    </nav>
                <!-- END .wrapper -->
                </div>
                <!-- BEGIN .image-slider -->
            <!-- END .header -->
            </header>
            <!-- BEGIN .content -->
            <section class="content">
                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <!-- BEGIN .with-sidebar-layout -->
                    <div class="with-sidebar-layout left">
                        <div class="content-panel">
                                <div class="panel-title">
                                        <h2>Phim Đang Chiếu</h2>
                                        <div class="right video-set-layout">
                                                <a href="#v-set-layout" rel="grid" class="active"><i class="fa fa-th"></i></a>
                                                <a href="#v-set-layout" rel="hdgrid"><i class="fa fa-th-large"></i></a>
                                                <a href="#v-set-layout" rel="list"><i class="fa fa-th-list"></i></a>
                                                <a href="#v-set-layout" rel="hdlist"><i class="fa fa-bars"></i></a>
                                        </div>
                                </div>
                                <div class="panel-block video-list grid">

                                        <!-- BEGIN .item -->
                                        <?php foreach ($list_films_showing as $film) {?>
                                            
                                        <div class="item">
                                                <div class="item-header">
                                                    <a href="<?php echo DOMAIN?>index.php?controller=film&amp;action=detail&amp;id=<?php echo $film['id']?>" class="img-hover-effect "><img src="<?php echo URL_IMAGE.$film['image'];?>" width="16" height="9" class="aspect-px" rel="" alt="" /></a>
                                                </div>
                                                <div class="item-content">
                                                        <h3><a href="#"><?php echo $film['name']?></a></h3>
                                                        <span class="video-meta">
                                                                <a href="#"><i class="fa fa-comment"></i>283</a>
                                                                <a href="post.html"><i class="fa fa-eye"></i><?php echo $film['views']?></a>
                                                                <a href="#"><i class="fa fa-heart"></i>95</a>
                                                        </span>
                                                        <p><?php echo $film['introduce']?></p>
                                                </div>
                                        <!-- END .item -->
                                        </div>
                                        <?php }?>
<!--                                        <div class="clear-list-button">
                                                <a href="browse.html" class="back-button">View More Videos</a>
                                        </div>-->

                                </div>
                        </div>

                        <div class="content-panel">
                                <div class="panel-block banner-block">
                                        <a href="#" target="_blank"><img src="<?php echo URL_IMAGE?>no-banner-468x60-light.jpg" width="468" height="60" alt=""></a>
                                        <a href="contact-us.html" class="banner-meta"><i class="fa fa-angle-double-up"></i> Contact us about advert spaces <i class="fa fa-angle-double-up"></i></a>
                                </div>
                        </div>

                        <div class="content-panel">
                                <div class="panel-title">
                                        <h2>Phim Sắp Chiếu</h2>
                                        <div class="right video-set-layout">
                                                <a href="#v-set-layout" rel="grid"><i class="fa fa-th"></i></a>
                                                <a href="#v-set-layout" rel="hdgrid" class="active"><i class="fa fa-th-large"></i></a>
                                                <a href="#v-set-layout" rel="list"><i class="fa fa-th-list"></i></a>
                                                <a href="#v-set-layout" rel="hdlist"><i class="fa fa-bars"></i></a>
                                        </div>
                                </div>
                                <div class="panel-block video-list hdgrid">
                                    <!-- BEGIN .item -->
                                    <?php foreach ($list_films_new as $film) {?>
                                            
                                        
                                        <div class="item">
                                                <div class="item-header">
                                                    <a href="<?php echo DOMAIN.'index.php?controller=film&amp;action=detail&amp;id='.$film['id']?>" class="img-hover-effect ">
                                                        <img src="<?php echo URL_IMAGE.$film['image'];?>" width="16" height="9" class="aspect-px" rel="" alt="" />
                                                    </a>
                                                </div>
                                                <div class="item-content">
                                                        <h3><a href="#"><?php echo $film['name']?></a></h3>
                                                        <span class="video-meta">
                                                                <a href="#"><i class="fa fa-comment"></i>283</a>
                                                                <a href="post.html"><i class="fa fa-eye"></i><?php echo $film['views']?></a>
                                                                <a href="#"><i class="fa fa-heart"></i>95</a>
                                                        </span>
                                                        <p><?php echo $film['introduce']?></p>
                                                </div>
                                        <!-- END .item -->
                                        </div>
                                        <?php }?>
<!--                                        <div class="clear-list-button">
                                                <a href="browse.html" class="back-button">View More Videos</a>
                                        </div>-->
                                </div>
                        </div>
                        <div class="content-panel">
                                <div class="panel-block banner-block">
                                        <a href="#" target="_blank"><img src="<?php echo URL_IMAGE?>no-banner-468x60-light.jpg" width="468" height="60" alt=""></a>
                                        <a href="contact-us.html" class="banner-meta"><i class="fa fa-angle-double-up"></i> Contact us about advert spaces <i class="fa fa-angle-double-up"></i></a>
                                </div>
                        </div>
                    <!-- END .with-sidebar-layout -->
                    </div>
                    <!-- BEGIN #sidebar -->
                    <aside id="sidebar" class="right">
                        <!-- BEGIN .widget -->
                        <div class="widget">
                            <div class="banner-widget no-border">
                                <a href="#" target="_blank"><img src="<?php echo URL_IMAGE?>no-banner-300x250.jpg" width="300" height="250" alt="" /></a>
                                <a href="contact-us.html" class="banner-meta"><i class="fa fa-angle-double-up"></i> Contact us about advert spaces <i class="fa fa-angle-double-up"></i></a>
                            </div>
                        <!-- END .widget -->
                        </div>

                        <!-- BEGIN .widget -->
                        <div class="widget">
                            <h3>Phim Xem Nhiều</h3>
                            <div class="widget-videos">
                                <?php foreach ($list_films_high_views as $film) {?>
                                <!-- BEGIN .item -->
                                <div class="item">
                                    <div class="item-header">
                                        <a href="#" class="video-thumb "><img src="<?php echo URL_IMAGE.$film['image']?>" width="16" height="9" style="height: 110px;width: 80px;" class="aspect-px" rel="" alt="" /></a>
                                        <ul style="position: absolute;top: 15px;left: 95px;">
                                            <li style="margin-bottom: 10px;"><span><strong>Thể loại: </strong><?php echo $film['typefilm']?></span></li>
                                            <li style="margin-bottom: 10px;"><span><strong>Thời lượng: </strong><?php echo $film['duration']?></span></li>
                                            
                                        </ul>
                                    </div>
                                    <div class="item-content">
                                        <h3><a href="post.html"><?php echo $film['name']?></a></h3>
                                        <span class="video-meta">
                                            <!--<a href="#"><i class="fa fa-comment"></i>283</a>-->
                                            <a href="#"><i class="fa fa-eye"></i><?php echo $film['views']?></a>
                                            <a href="#"><i class="fa fa-heart"></i>95</a>
                                        </span>
                                    </div>
                                <!-- END .item -->
                                </div>
                                <?php }?>
                            </div>
                        <!-- END .widget -->
                        </div>

<!--                         BEGIN .widget 
                        <div class="widget">
                            <h3>Popular Videos</h3>
                            <div class="widget-videos-small">

                                 BEGIN .item 
                                <div class="item">
                                    <div class="item-header">
                                        <a href="post.html" class="video-thumb "><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/580/274/58027488_295.jpg" alt="" /></a>
                                    </div>
                                    <div class="item-content">
                                        <a href="post.html"><span class="marker">Creative Stuff</span></a>
                                        <h3><a href="post.html">Television is a drug.</a></h3>
                                        <span class="video-meta">
                                            <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                            <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                            <a href="#"><i class="fa fa-heart"></i>95</a>
                                        </span>
                                    </div>
                                 END .item 
                                </div>
                            </div>
                         END .widget 
                        </div>-->
                        <!-- BEGIN .widget -->
                        <div class="widget">
                            <div class="banner-widget no-border">
                                <a href="#" target="_blank"><img src="<?php echo URL_IMAGE?>no-banner-300x250.jpg" width="300" height="250" alt="" /></a>
                                <a href="#" class="banner-meta"><i class="fa fa-angle-double-up"></i> Contact us about advert spaces <i class="fa fa-angle-double-up"></i></a>
                            </div>
                        <!-- END .widget -->
                        </div>

                        <!-- BEGIN .widget -->
                        <div class="widget">
                            <h3>Tag Cloud</h3>
                            <div class="tagcloud">
                                <a href="#">Creative</a>
                                <a href="#">Neglegentur</a>
                                <a href="#">Gubergren</a>
                                <a href="#">Temporibus reformid</a>
                                <a href="#">Democ</a>
                                <a href="#">Gubergren</a>
                                <a href="#">Neglegentur</a>
                                <a href="#">Creative</a>
                            </div>
                        <!-- END .widget -->
                        </div>

<!--                         BEGIN .widget 
                        <div class="widget">
                            <h3>Latest Comments</h3>
                            <div class="comments-list">
                                <div class="item">
                                    <div class="item-header">
                                        <img src="<?php echo URL_IMAGE?>photos/image-1.jpg" width="40" height="40" class="item-photo" alt="" />
                                        <div class="item-header-title">
                                            <h4>Radclyffe Mariano<span>says:</span></h4>
                                            <span class="datetime">July 6th 2013, 12:00</span>
                                        </div>
                                    </div>
                                    <div class="item-content">
                                        <p>Eos alii duis comprehensam ea. Ad vix sumo tim eam petentium, soluta corpora mnesarchum ex nemore everti dolorem at, eu mazim.</p>
                                        <a href="post.html#comments" class="small-link"><i class="fa fa-comment"></i>View Comment</a>
                                    </div>
                                </div>
                            </div>
                         END .widget 
                        </div>-->
                        <!-- BEGIN .widget -->
<!--                        <div class="widget">
                            <h3>Menu</h3>
                            <ul>
                                <li><a href="browse.html">Lorem Ipsum</a> (9)</li>
                                <li><a href="browse.html">Lalalala</a> (9)</li>
                                <li><a href="browse.html">Lorem Ipsum</a> (9)</li>
                                <li><a href="browse.html">Lalalala</a> (9)</li>
                            </ul>
                         END .widget 
                        </div>
                         BEGIN .widget 
                        <div class="widget">
                            <h3>Custom Video Cateogries</h3>
                            <ul class="custom-categories">
                                <li><a href="browse.html">Music Videos<span>327</span></a></li>
                                <li><a href="browse.html">Creative Stuff<span>182</span></a></li>
                                <li><a href="browse.html">Vines<span>182</span></a></li>
                                <li><a href="browse.html">HD Videos<span>182</span></a></li>
                                <li><a href="browse.html">Experimental<span>32</span></a></li>
                            </ul>
                         END .widget 
                        </div>-->
                    <!-- END #sidebar -->
                    </aside>
                <!-- END .wrapper -->
                <div id="side-left" >
                            
                <a href="#" target="_blank">                    
                    <img src="<?php echo URL_IMAGE?>ad1.jpg" width="150" height="800">    
                </a>
                </div>
                <div id="side-right" > 
                            
                <a href="#" target="_blank">                    
                    <img src="<?php echo URL_IMAGE?>ad2.jpg" width="150" height="800">    
                </a>
            	</div>
                </div>
            <!-- BEGIN .content -->
            </section>
            <!-- BEGIN .footer -->
            <footer class="footer">
                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <h3>Popular Videos</h3>
                        <div class="widget-videos-small">
                            <!-- BEGIN .item -->
                            <div class="item">
                                <div class="item-header">
                                    <a href="post.html" class="video-thumb "><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://i.vimeocdn.com/video/435803636_295x166.jpg" alt="" /></a>
                                </div>
                                <div class="item-content">
                                    <a href="browse.html"><span class="marker">HD Videos</span></a>
                                    <h3><a href="post.html">Nuit Blanche</a></h3>
                                    <span class="video-meta">
                                        <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                        <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                        <a href="#"><i class="fa fa-heart"></i>95</a>
                                    </span>
                                </div>
                            <!-- END .item -->
                            </div>
                            <!-- BEGIN .item -->
                            <div class="item">
                                <div class="item-header">
                                    <a href="post.html" class="video-thumb "><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/500/390/50039066_295.jpg" alt="" /></a>
                                </div>
                                <div class="item-content">
                                    <a href="post.html"><span class="marker">Creative Stuff</span></a>
                                    <h3><a href="post.html">Nokta.</a></h3>
                                    <span class="video-meta">
                                        <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                        <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                        <a href="#"><i class="fa fa-heart"></i>95</a>
                                    </span>
                                </div>
                            <!-- END .item -->
                                </div>
                            <!-- BEGIN .item -->
                            <div class="item">
                                <div class="item-header">
                                    <a href="post.html" class="video-thumb "><img src="<?php echo URL_IMAGE?>aspect-px.png" width="16" height="9" class="aspect-px" rel="http://b.vimeocdn.com/ts/244/196/244196772_295.jpg" alt="" /></a>
                                </div>
                                <div class="item-content">
                                    <a href="post.html"><span class="marker">Experimental</span></a>
                                    <h3><a href="post.html">PROTEIGON</a></h3>
                                    <span class="video-meta">
                                        <a href="post.html"><i class="fa fa-comment"></i>283</a>
                                        <a href="post.html"><i class="fa fa-eye"></i>829</a>
                                        <a href="#"><i class="fa fa-heart"></i>95</a>
                                    </span>
                                </div>
                            <!-- END .item -->
                            </div>
                        </div>
                    <!-- END .widget -->
                    </div>
                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <h3>Tag Cloud</h3>
                        <div class="tagcloud">
                            <a href="browse.html">Creative</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Temporibus reformid</a>
                            <a href="browse.html">Democ</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Creative</a>
                            <a href="browse.html">Creative</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Temporibus reformid</a>
                            <a href="browse.html">Democ</a>
                            <a href="browse.html">Gubergren</a>
                            <a href="browse.html">Neglegentur</a>
                            <a href="browse.html">Creative</a>
                        </div>
                    <!-- END .widget -->
                    </div>
                    <!-- BEGIN .widget -->
                    <div class="widget">
                        <h3>Contact Us</h3>
                        <div class="htmlcode">
                            <p>HDcinema.com is a website to booking film ticket.Let's enjoy your's life with HDcinema.com</p>
                            <ul>
                                <li><span class="small-text">Address</span><h6>122 Tran Phu Street, Hai Chau<br/>Da Nang, Viet Nam</h6></li>
                                <li><span class="small-text">Phone number</span><h6>0511 241 3300</h6></li>
                                <li><span class="small-text">E-mail address</span><h6>support@gmail.com</h6></li>
                            </ul>
                        </div>
                    <!-- END .widget -->
                    </div>
                <!-- END .wrapper -->
                </div>
            <!-- END .footer -->
            </footer>
            <div class="footer-bottom">
                <!-- BEGIN .wrapper -->
                <div class="wrapper">
                    <ul class="right">
                        <li><a href="index-2.html">Home</a></li>
                        <li><a href="browse.html">Browse</a></li>
                        <li><a href="browse.html">Popular</a></li>
                        <li><a href="sample.html">Sample Page</a></li>
                        <li><a href="contact-us.html">Contact Us</a></li>
                    </ul>
                    <p>&copy; 2014 Copyright <b>HDcinema</b>. All Rights reserved.</p>
                <!-- END .wrapper -->
                </div>
            </div>
        <!-- END .boxed -->
        </div>
        <script>
            $(document).ready(function () {
                // Video set layout
		$("a[href='#v-set-layout']").click(function(){
			var element = $(this);
			element.addClass("active").siblings(".active").removeClass("active");
			element.parent().parent().siblings(".panel-block").attr("class", "panel-block video-list").addClass(element.attr("rel"));
			return false;
		});
                });
            </script>
        <!-- Scripts -->
        <script type="text/javascript" src="<?php echo URL_JS?>ot-menu.js"></script>
    </body>
</html>