<?php include 'views/layout/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <p class="form-title">
                    Sign In</p>
                <form class="login" method="POST">
                    <input type="text" name="username" value="<?php echo DB::get_post('username', '')?>" placeholder="Username" />
                    <span><?php echo isset($errors['username'])? $errors['username']:''?></span>
                    <input type="password" name="password" value="<?php echo DB::get_post('password', '')?>" placeholder="Password" />
                    <span><?php echo isset($errors['password'])? $errors['password']:''?></span>
                <input type="submit" value="Sign In" class="btn btn-success btn-sm" />
                <div class="remember-forgot">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" />
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 forgot-pass-content">
                            <a href="<?php echo DOMAIN_AD?>index.php?controller=auth&action=forgotpass" class="forgot-pass">Forgot Password</a>
                        </div>
                    </div>
                </div>
                </form>
            </div>
            <div style="width: 250px;margin: auto">
                <p style="color: white;font-size: 14px;"><?php echo isset($errors['admin'])? $errors['admin']:''?></p>
                <p style="color: white;font-size: 14px;"><?php echo isset($errors['common'])? $errors['common']:''?></p>
            </div>
        </div>
    </div>
</div>
<?php include 'views/layout/footer.php'; ?>