/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

        
        var seatParent = Request.QueryString("seatParent");
        if (seatParent == "")
            seatParent = 0;
        var seatChild = Request.QueryString("seatChild");
        if (seatChild == "")
            seatChild = 0;
        var seatTotal = parseFloat(seatParent) + parseFloat(seatChild);
        var seatCoupleFirst = 0;
        var seatCoupleSecond = 0;
        var totalSeat = 1;

      +  function selectSeat(seatID) {
            // xử lý check chọn ghế
            var _value = document.getElementById(seatID).className;
            if (_value == "item-seat") {
                document.getElementById(seatID).className = "item-seat-selected";
            }
            else if (_value == "item-seat-vip") {
                document.getElementById(seatID).className = "item-seat-selected-vip";
            }
            else if (_value == "item-seat-selected") {
                document.getElementById(seatID).className = "item-seat";
            }
            else if (_value == "item-seat-selected-vip") {
                document.getElementById(seatID).className = "item-seat-vip";
            }
            // choose seat couple
            else if (_value == "item-seat-couple") {
                if (totalSeat == 1)
                {
                    alert('Ghế couple chỉ dành khi đặt 2 vé trở lên. Xin vui lòng chọn ghế thường.'); return;
                }

                if (seatCoupleFirst == 0)
                {
                    // set to flag
                    seatCoupleFirst = seatID.substring(2);
                }
                else if (seatCoupleFirst != 0) // second seat
                {
                    var currentSeat = seatID.substring(2);
                    var msg = "Ghế couple phải chọn liền kề nhau.";

                    // kiểm tra nếu là số lẻ
                    if ((seatCoupleFirst % 2) > 0) {
                        if (currentSeat - seatCoupleFirst != 1) {
                            alert(msg); return;
                        }
                    }
                    else // số chẵn 
                    {
                        if (seatCoupleFirst - currentSeat != 1) {
                            alert(msg); return;
                        }
                    }
                    seatCoupleSecond = currentSeat;
                }
                // set class
                document.getElementById(seatID).className = "item-seat-selected-couple";
            }
            // remove seat couple
            else if (_value == "item-seat-selected-couple") {
                document.getElementById(seatID).className = "item-seat-couple";

                // -- reset
                var currentSeat = seatID.substring(2);
                if (currentSeat == seatCoupleFirst)
                {
                    seatCoupleFirst = 0;
                }
                if (currentSeat == seatCoupleSecond) {
                    seatCoupleSecond = 0;
                }

            }           
            var seatCount = 0;
            var a = document.getElementsByTagName('div');           
            for (i = 0; i < a.length; i++) {
                if (a.item(i).className == "item-seat-selected" || a.item(i).className == "item-seat-selected-vip") {
                    seatCount += 1;
                }
            }           
                    
            if (seatCount > seatTotal) {
                var _classSeat = document.getElementById(seatID).className;
                if (_classSeat == "item-seat-selected-vip") {
                    document.getElementById(seatID).className = "item-seat-vip";
                }
                else if (_classSeat == "item-seat-selected") {
                    document.getElementById(seatID).className = "item-seat";
                }              
                      
                alert('You only select ' + parseFloat(seatTotal) + ' Seat');
                return false;
            }

        }
      +  function GetListSeat() {
            var a = document.getElementsByTagName('div');
            var _listSeat = "";
            for (i = 0; i < a.length; i++) {
                if (a.item(i).className == "item-seat-selected" || a.item(i).className == "item-seat-selected-vip" || a.item(i).className == "item-seat-selected-couple") {
                    _listSeat += a.item(i).id + ",";
                }
            }
            return _listSeat;
        }
      +  function Continue() {

            var seatCount = 0;
            var a = document.getElementsByTagName('div');
            for (i = 0; i < a.length; i++) {
                if (a.item(i).className == "item-seat-selected" || a.item(i).className == "item-seat-selected-vip" || a.item(i).className == "item-seat-selected-couple") {
                    seatCount += 1;
                }
            }
                ////////////////////////////////
    //        // Đếm số lượng ghế trống giữa hai ghế
    //        var countCenter = 0;
    //        for (i = 0; i < a.length; i++) {
    //            if (a.item(i).className == "item-seat-selected" && a.item(i + 1).className != "item-seat-selected" && a.item(i + 2).className == "item-seat-selected") {
    //                countCenter += 1;
    //            }
    //            if (a.item(i).className == "item-seat-selected" && a.item(i + 1).className != "item-seat-selected" && a.item(i + 2).className == "item-seat-selected") {
    //                countCenter += 1;
    //            }
    //        }
    //        if (countCenter != 0) {               
    //            alert('You can not select seats leaving an empty seat in the middle');
    //            return false;
    //        }                        
    //        ///////////// Đếm ghế trống và biên phải
    //        var _countEmptyRight = 0;
    //        for (i = 0; i < a.length; i++) {
    //          if (a.item(i).className == "item-seat-selected" && a.item(i + 1).className == "item-seat" && a.item(i + 1).getAttribute("item-right") == "item-right") {
    //             _countEmptyRight += 1;
                    
    //              }
    //         else if (a.item(i).className == "item-seat-selected" && a.item(i).getAttribute("item-left") == "item-left" && a.item(i + 1).className == "item-seat-selected" && a.item(i + 1).getAttribute("item-right") == "item-right") {
    //             _countEmptyRight = 0;
    //         }
    //        }
    //        if (_countEmptyRight != 0) {
    //           alert('You can not select seats leaving an empty seat in the middle');
    //          return false;
    //        }
                     
    //        ///////////// Đếm ghế trống và biên trái
    //        var _countEmptyLeft = 0;
    //        for (i = 0; i < a.length; i++) {
    //            if (a.item(i).className == "item-seat-selected" && a.item(i - 1).className == "item-seat" && a.item(i - 1).getAttribute("item-left") == a.item(i).getAttribute("item-left")) {
    //            _countEmptyLeft += 1;
                    
    //            }
    //         else if (a.item(i).className == "item-seat-selected" && a.item(i).getAttribute("item-right") == "item-right" && a.item(i - 1).className == "item-seat-selected" && a.item(i - 1).getAttribute("item-left") == "item-left") {
    //            _countEmptyLeft = 0;
    //         }

    //         else if (a.item(i).className == "item-seat-selected" && a.item(i).getAttribute("item-left") == "item-left" && a.item(i - 1).className == "item-seat-selected" && a.item(i - 1).getAttribute("item-right") == "item-right" && a.item(i - 2).getAttribute("item-left") == "item-left") {
    //             _countEmptyLeft += 1;
    //         }

    //         }
    //        if (_countEmptyLeft != 0) {
    //         alert('You can not select seats leaving an empty seat in the middle');
    //         return false;
    //         }

    //        //alert(_strList);
    //        //return false;

            //////////// Ghế chọn vượt quá giới hạng cho phép hoặc thiếu
            if (seatCount > seatTotal) {
                alert('You only select' + parseFloat(seatTotal) + 'Seat');
                return false;
            }
            if (seatCount < seatTotal) {
                alert('Please select  ' + (seatTotal - seatCount) + ' seat');                
                return false;
            }
            var _strList = GetListSeat();                       
            if (_strList != "") {               
                PageMethods.SendForm(_strList, OnSucceeded, OnFailed);
            }
            else {
                alert('Please select seat');
                return false;
            }
        }

       + function GoBack() {
            var query = "/Buoc-1.aspx";
            window.location = query;
        }

      +  function OnSucceeded() {
            window.location = "/Buoc-3.aspx";
        }

      +  function OnFailed(error) {
            //alert(error.get_message());
        }

       + function CheckRapVip(idVip, price) {
            //var seatParent = Request.QueryString("seatParent");
            //var seatChild = Request.QueryString("seatChild");
            //var seatTotal = parseFloat(seatParent) + parseFloat(seatChild);
            var seatCount = 0;
            var a = document.getElementsByTagName('div');
            for (i = 0; i < a.length; i++) {
                if (a.item(i).className == "item-seat-selected-vip") {
                    seatCount += 1;
                }
            }            
            if (document.getElementById(idVip).className == "item-seat-selected-vip") {
                var _total = document.getElementById("ctl00_plcMain_lblTongTien").innerHTML.replace(",", "").replace(".", "");
                var totalvip = parseFloat(_total) + parseFloat(price *= -1);
                document.getElementById("ctl00_plcMain_lblTongTien").innerHTML = accounting.formatNumber(totalvip);  //totalvip;              
            }
            else {
                if (seatCount == seatTotal) {
                    if (document.getElementById(idVip).className != "item-seat-vip") {
                        var _total = document.getElementById("ctl00_plcMain_lblTongTien").innerHTML.replace(",", "").replace(".", "");
                        var totalvip = parseFloat(_total) - parseFloat(price *= -1);
                        document.getElementById("ctl00_plcMain_lblTongTien").innerHTML = accounting.formatNumber(totalvip);
                    }
                }
                else {
                    var _total = document.getElementById("ctl00_plcMain_lblTongTien").innerHTML.replace(",", "").replace(".", "");
                    var totalvip = parseFloat(_total) - parseFloat(price *= -1);
                    document.getElementById("ctl00_plcMain_lblTongTien").innerHTML = accounting.formatNumber(totalvip);
                }                           
            }                 
        }
    
    
