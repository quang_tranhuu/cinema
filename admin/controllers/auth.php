<?php

require_once 'models/User_model1.php';

$action = isset($_GET['action']) ? $_GET['action'] : 'index';

switch ($action) {
    case 'login':
        $errors = [];
        
        if (isset($_POST) && count($_POST) > 0) {
            if (empty($_POST['username'])) {
                $errors['username'] = 'Required';
            }
            if (empty($_POST['password'])) {
                $errors['password'] = 'Required';
            }
            if (empty($errors)) {
                $user = User_model1::get_user($_POST['username']);
                if(!empty($user)){
                    if (($user[0]['password'] == $_POST['password'])) {
                        if($user[0]['role']==1){
                            // Store session
                            $_SESSION['user_id'] = $user[0]['id'];
                            $_SESSION['username'] = $user[0]['username'];
                            $_SESSION['image']=$user[0]['image'];
                            $_SESSION['name']=$user[0]['name'];
                            $_SESSION['role']=$user[0]['role'];
                            header('Location: ' . DOMAIN .'admin/index.php');
                        } else {
                            $errors['admin']='Your account is not admin. Please check again!';
                        }
                    } else {
                        $errors['password'] = 'Password is invalid';
                    }
                }else {
                    $errors['common'] = 'Username or password is invalid';
                    print_r($errors);
                }
            }
        }
        
        include('views/login.php');
        break;
    case 'logout':
        session_destroy();
        header('Location: ' . DOMAIN .'index.php');
        break;
    case 'forgotpass':
        if (isset($_POST) && count($_POST) > 0) {
            if (empty($_POST['email'])) {
                $errors['email'] = 'Required';
            }  else {
                $user=DB::get_where('users', array(
                                    'email'=> $_POST['email']
                                 ));
                if(empty($user)){
                    $errors['email']='Wrong email';
                }
            }
            

            if(empty($errors)){
                $des=$_POST['email'];
                $title='Forgot Password';
                $content='Bạn đã yêu cầu lấy lại mật khẩu. Sử dụng mật khẩu sau để đăng nhập :'.$user[0]['password'];
                DB::sendMail($des, $content, $title);
                $mes='Thành công ! Xin vui lòng vào email của bạn để kiểm tra và đăng nhập.';
            }
        }  
        include ('views/forgotpass.php');
        break;
}

