<?php

require_once 'models/user_model.php';

$action = isset($_GET['action']) ? $_GET['action'] : 'index';

switch ($action) {
    case 'login':
        $errors = [];
        
        if (isset($_POST) && count($_POST) > 0) {
            if (empty($_POST['username'])) {
                $errors['username'] = 'Required';
            }
            if (empty($_POST['password'])) {
                $errors['password'] = 'Required';
            }
            if (empty($errors)) {
                $user = User_model::get_user($_POST['username']);
                if (!empty($user) && ($user[0]['password'] == $_POST['password'])) {
                    // Store session
                    $_SESSION['user_id'] = $user[0]['id'];
                    $_SESSION['username'] = $user[0]['username'];
                    $_SESSION['image']=$user[0]['image'];
                    $_SESSION['name']=$user[0]['name'];
                    $_SESSION['role']=$user[0]['role'];
                    print_r($_SESSION);
                    header('Location: ' . DOMAIN .'index.php');
                } else {
                    $errors['common'] = 'Username or password is invalid';
                }
            }
        }
        include('views/login.php');
        break;
    case 'logout':
        session_destroy();
        header('Location: ' . DOMAIN .'index.php');
        break;
    case 'forgotpass':
        if (isset($_POST) && count($_POST) > 0) {
            if (empty($_POST['email'])) {
                $errors['email'] = 'Required';
            }else {
                $user=DB::get_where('users', array(
                                    'email'=> $_POST['email']
                                 ));
                if(empty($user)){
                    $errors['email']='Wrong email';
                }
            }
            
            if(empty($errors)){
                $des=$_POST['email'];
                $title='Forgot Password';
                $content='Bạn đã yêu cầu lấy lại mật khẩu. Sử dụng mật khẩu sau để đăng nhập :'.$user[0]['password'];
                DB::sendMail($des, $content, $title);
                $mes='Thành công ! Xin vui lòng vào email của bạn để kiểm tra và đăng nhập.';
            }
        }
        include ('views/forgotpass.php');
        break;
    case 'register':
        $errors = [];
        
        if (isset($_POST) && count($_POST) > 0) {
            if ($_POST['username'] == '') {
                $errors['username'] = 'Required';
            }else {
                $user=  DB::get_where('users',array('username'=> $_POST['username']));
                if(!empty($user)){
                    if ($_POST['username']==$user[0]['username']) {
                    $errors['username']='Username exist';
                    }
                }
            }
            if ($_POST['password'] == '') {
                $errors['password'] = 'Required';
            }else if(strlen($_POST['password'])<6){
                $errors['password']='Must be than 6 character';
            }
            if($_POST['repassword']==''){
                $errors['repassword'] = 'Required';
            }  else if($_POST['repassword']!=$_POST['password']){
                $errors['repassword']='Must equal password';
            }
            if($_POST['email']==''){
                $errors['email'] = 'Required';
            }else {
                $okay = preg_match('/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $_POST['email']);
                if(!$okay){
                $errors['email'] = 'Invalid email format. Exp: abc@gmail.com';
                
                }else {
                    $user=  DB::get_where('users',array('email'=>$_POST['email']));
                    if(!empty($user)){
                        if ($_POST['email']==$user[0]['email']) {
                        $errors['email']='Email exist';
                        }
                    }
            }
            }
            
            if(empty($errors)){
                DB::insert('users', array(
                    'username'=> $_POST['username'],
                    'password'=> $_POST['password'],
                    'email'=> $_POST['email'],
                    'role'=> '0',
                ));
                $mes='Register success ! ';
//                $button="<a href='".DOMAIN."index.php?controller=auth&action=login' class='btn btn-default'>Login</button>";
            }
        }
        include ('views/register.php');
        break;
}

