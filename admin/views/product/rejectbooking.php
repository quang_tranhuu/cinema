<?php include 'views/layout/header_admin.php' ?>
<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content">
        <form method="POST" action="">
         <div class="content-panel" >
                <div class="panel-title">
                    <h2>Reject Booking </h2>
                </div>
                <div class="panel-block" style="width: 70%;margin: auto">
                    <div class="form-group">
                        <label class="col-md-3 control-label " for="name" style="font-size: 18px" ><b>Lí do :</b></label>  
                    <div class="col-md-6">
                        <textarea id="reason" name="reason"  placeholder="Enter reason" class="form-control input-md" ></textarea>
                    </div>
                    <div class="col-md-3" style="color: red">
                        <span>*<?php echo isset($errors['reason'])? $errors['reason']: '';?></span>
                    </div>
                    </div>
                    <div class="clearfix" style="margin-bottom: 30px"></div>
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4">
                            <button id="" type="submit" class="btn btn-primary" >Send</button>
                        </div>
                    </div>
                    <span>*<?php echo isset($mesage)? $mesage: '';?></span>
                </div>
         </div>
        </form>
    </div>

</div>
<?php include 'views/layout/footer_admin.php' ?>