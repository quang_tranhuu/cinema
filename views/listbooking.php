<?php include 'views/layout/header_personpage.php' ?>
<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content">
        <div class="row">
        <div class="col-md-8">
        <h1> BOOKING</h1>
    </div>
    
    <div class="col-md-12">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>STT</th> 
                    <th>Người đặt</th> 
                    <th>Phim</th> 
                    <th>Suất</th> 
                    <th>Ngày</th> 
                    <th>Ghế</th>
                    <th>Trạng thái</th>
                    <th>Thông báo</th>
                </tr> 
            </thead> 
            <tbody> 
                <?php foreach($list_booking as  $item) { ?>
                <tr> 
                    <td><?php echo $item['id'] ?></td> 
                    <td><?php echo $item['user_name'] ?></td>
                    <td><?php echo $item['film_name'] ?></td>
                    <td><?php echo $item['time'] ?></td>
                    <td><?php echo $item['date'] ?></td>
                    <td><?php echo $item['seat'] ?></td>
                    <td><?php echo $item['status'] ?></td>
                    <td><?php echo $item['content'] ?></td>
                </tr> 
                <?php } ?>
            </tbody> 
        </table>
    </div>
    
        </div>
    </div>
</div>
<?php include 'views/layout/footer_personpage.php' ?>