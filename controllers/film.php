<?php
require_once ('models/Category_model.php');
require_once ('models/Film_cinema_model.php');
require_once ('models/Cinema_model.php');
require_once ('models/Day_film_model.php');
require_once ('models/Day_model.php');
require_once ('models/Film_model.php');
require_once ('models/Schedule_model.php');
require_once ('models/Hour_name_model.php');
require_once ('models/Booking_model.php');
require_once ('admin/models/User_model1.php');

$action = isset($_GET['action']) ? $_GET['action'] : 'index';
switch ($action) {
    case 'detail':
            
            $result=  Film_model::get_by_id($_GET['id']);
            DB::update('films', array('views'=> $result[0]['views']+1), $_GET['id']);
        include ('views/detail_film_page.php');
        break;

    case 'home':
        if(isset($_GET)&& count($_GET)>0){
            $list_films_new=  Film_model::get_by_idcategory('2');
            $list_films_showing=  Film_model::get_by_idcategory('1');
        }
        include ('views/homepage/homepage.php');
        break;
    case 'list_booking':
        $list_booking = DB::get_where('bookings',array(
                                        'id_user'=> $_SESSION['user_id'],
                                      ));
        include('views/listbooking.php');
        break;
    default:
        break;
}
