<?php include 'views/layout/header_admin.php' ?>

<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content">
        <div class="row">
        <div class="col-md-8">
        <h1>MANAGE MEMBER</h1>
    </div>
    
    <div class="col-md-12">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>Index</th> 
                    <th>Username</th> 
                    <th>Tel</th> 
                    <th>Address</th> 
                    <th>Email</th> 
                    
                    <th>Action</th>
                </tr> 
            </thead> 
            <tbody> 
                <?php foreach($list_user as  $user) { ?>
                <tr> 
                    <th scope="row"><?php echo ++$offset; ?></th> 
                    <td><?php echo $user['username'] ?></td>
                    <td><?php echo $user['tel'] ?></td>
                    <td><?php echo $user['address'] ?></td>
                    <td><?php echo $user['email'] ?></td>
                    <td>
                        <a href="<?php echo DOMAIN_AD .'index.php?controller=user&action=edit&id=' . $user['id'] ?>"><i class="fa fa-pencil-square-o "></i></a> &nbsp;
                        <a href="<?php echo DOMAIN_AD .'index.php?controller=user&action=delete&id=' . $user['id'] ?>"><i class="fa fa-trash"></i></a> 

                    </td>
                </tr> 
                <?php } ?>
            </tbody> 
        </table>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
        <nav>
            <?php echo Pagination::render($total_record, $limit, $page, DOMAIN . 'admin/index.php?controller=user&action=list_user&page='); ?>
        </nav>
        </div>
        <div class="col-md-offset-2 col-md-4 add">
            <a  href="<?php echo DOMAIN .'admin/index.php?controller=user&action=create'; ?>" class="btn btn-primary "><i class="fa fa-plus-circle fa-">Tạo user mới</i></a>
        </div>
    </div> 
        </div>
    </div>
</div>
<?php include 'views/layout/footer_admin.php' ?>