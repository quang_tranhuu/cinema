<?php include 'views/layout/header1.php'; ?>

</header>
<!-- BEGIN .content -->
<section class="content" style="padding-top: 0px">

    <!-- BEGIN .wrapper -->
    <div class="wrapper">

        <!-- BEGIN .without-sidebar-layout -->
        <div class="without-sidebar-layout">
            <form action="<?php echo DOMAIN;?>index.php?controller=choice&action=form_fill" method="POST" id="choseseat">
            <div class="content-panel">
                <div class="panel-title">
                    <h2>Thông tin </h2>
                </div>
                <div class="panel-block">
                    <div class="panel-content">
                        <!--<div class="thongtin">-->
                        <p><strong>Rạp phim : </strong> <span><?php echo $_SESSION['cinema_name']; ?></span></p><span></span>
                        <p><strong>Tên phim : </strong><span><?php echo $_SESSION['film_name']; ?></span></p><span></span>
                        <p><strong>Ngày chiếu : </strong> <span><?php echo $_SESSION['day_name']; ?></span></p><span></span>
                        <p><strong>Suất chiếu : </strong><span><?php echo $_SESSION['time_name']; ?></span></p><span></span>
                        <p><strong>Số lượng vé: </strong><span><?php echo $_SESSION['quantity_ticket']; ?></span></p><span></span>
                        <p><strong>Tổng tiền : </strong><span><?php echo $_SESSION['total_price']; ?> VNĐ</span></p><span></span>
                        <p><strong>Seat: </strong><span><?php echo $_SESSION['seat_name']?></span></p>
                        <!--</div>-->
                        <!--<div class="">-->
                            
                        <!--</div>-->
                    </div>
                </div>
            </div>
                <div class="content-panel" >
                <div class="panel-title">
                    <h2>Thông tin người đặt </h2>
                </div>
                <div class="panel-block" style="width: 55%;margin: auto">
                    <div class="form-group">
                    <label class="col-md-3 control-label" for="username">Name</label>  
                    <div class="col-md-6">
                        <input id="username" name="username" value="<?php echo DB::get_post('username', '') ?>" type="text" placeholder="" class="form-control input-md" >
                    </div>
                    <div class="col-md-3" style="color: red">
                        *<?php echo isset($errors['name'])? $errors['name']: '';?>
                    </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="textinput" >ID CARD</label>  
                        <div class="col-md-6">
                            <input id="textinput" name="id_card" value="<?php echo DB::get_post('id_card', '') ?>"  type="text" placeholder="" class="form-control input-md">

                        </div>
                        <div class="col-md-3" style="color: red">
                            *<?php echo isset($errors['id_card'])? $errors['id_card']: '';?>
                        </div>
                    </div>
                     <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="tel">Tel</label>  
                        <div class="col-md-6">
                            <input id="tel" name="tel" value="<?php echo DB::get_post('tel', '') ?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            *<?php echo isset($errors['tel'])? $errors['tel'] : '';?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="email">Email</label>  
                        <div class="col-md-6">
                            <input id="email" name="email" value="<?php echo DB::get_post('email', '')  ?>"  type="email" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            *<?php echo isset($errors['email'])? $errors['email'] :'';?>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                    
                <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4">
                            <button id="" type="submit" class="btn btn-primary" >Submit</button>
                        </div>
                    </div>
                <div style="color: red"> <?php echo isset($message)? $message:''?></div>
                </div>
                
            </div>
            </form>
        </div>
    </div>
</section>
<?php include 'views/layout/footercinema.php'; ?>

