<?php include 'views/layout/header.php' ?>
<div class="row">
    <div class="col-md-8">
        <h1>PRODUCT MANAGER</h1>
    </div>
    <div class="col-md-4">
        <a href="<?php echo DOMAIN .'admin/index.php?controller=product&action=create'; ?>"><i class="fa fa-plus-circle fa-3x"></i></a>
    </div>
    <div class="col-md-12">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>#</th> 
                    <th>Product name</th> 
                    <th>Price</th> 
                    <th>Views</th> 
                    <th>Category</th> 
                    <th>Action</th>
                </tr> 
            </thead> 
            <tbody> 
                <?php foreach($list_products as $key => $product) { ?>
                <tr> 
                    <th scope="row"><?php echo $key + 1; ?></th> 
                    <td><?php echo $product['name'] ?></td>
                    <td><?php echo $product['price'] ?></td>
                    <td><?php echo $product['views'] ?></td>
                    <td><?php echo $product['category_name'] ?></td>
                    <td>
                        <a href="<?php echo DOMAIN .'admin/index.php?controller=product&action=edit&id=' . $product['id'] ?>"><i class="fa fa-pencil-square-o fa-2x"></i></a> &nbsp;&nbsp;&nbsp;
                        <a href="<?php echo DOMAIN .'admin/index.php?controller=product&action=delete&id=' . $product['id'] ?>"><i class="fa fa-trash fa-2x"></i></a> 
                    </td>
                </tr> 
                <?php } ?>
            </tbody> 
        </table>
    </div>
</div>
<?php include 'views/layout/footer.php' ?>