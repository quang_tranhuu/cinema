<?php
require_once ('models/User_model1.php');
switch ($action) {
    case 'edit':
        $user=  User_model1::get_user_by_id($_SESSION['user_id']);
        $errors =[];
        if (isset($_POST) && count($_POST) > 0) {
            // Validation
            if ($_POST['name'] == '') {
                $errors['name'] = 'Required';
            }
            if ($_POST['password'] == '') {
                $errors['password'] = 'Required';
            }else if($_POST['password']!= $user[0]['password']){
                $errors['password']='Password sai';
            } 
            if($_POST['passwordnew']==''){
                $errors['passwordnew'] = 'Required';
            }else if(strlen($_POST['passwordnew'])<6){
                $errors['passwordnew']='Must be than 6 character';
           }
            if($_POST['repassword']==''){
                $errors['repassword'] = 'Required';
            }else if ($_POST['repassword']!=$_POST['passwordnew']) {
                $errors['repassword']='Must equal password new';
                
            }
            if($_POST['tel']==''){
                $errors['tel'] = 'Required';
            }
            if($_POST['address']==''){
                $errors['address'] = 'Required';
            }
            if($_POST['email']==''){
                $errors['email'] = 'Required';
            }else {
                $okay = preg_match('/^[A-z0-9_\-]+[@][A-z0-9_\-]+([.][A-z0-9_\-]+)+[A-z.]{2,4}$/', $_POST['email']);
                if(!$okay){
                $errors['email'] = 'Invalid email format. Exp: abc@gmail.com';
                
                } 
            }
            if($_FILES['image']['name']==''){
                $errors['image'] = 'Required';
            }
            if (empty($errors)) {
                $extend=DB::checkTypeImage($_FILES['image']['type']);
                $namenew=PATH.$_SESSION['username'].$extend;
                if(file_exists($namenew)){              //check file exist
                    unlink($namenew);
                }
                $tmp_name = $_FILES['image']['tmp_name'];
                $success = move_uploaded_file($tmp_name, $namenew);
                $_SESSION['image']=$namenew;
                $_SESSION['name']=$_POST['name'];
                
                DB::update('users',array(
                    'name' => $_POST['name'],
                    'password' => $_POST['passwordnew'],
                    'tel' => $_POST['tel'],
                    'address' => $_POST['address'],
                    'email' => $_POST['email'],
                    'image' => $namenew,
                    
                ),$_SESSION['user_id']);
                $message="Update success !";
                
            }
        }
        include('views/product/edit_profile.php');
        break;
    case 'list_user':
        require_once '../library/Pagination.php';
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        
        // Get number of total product record in DB
        $total_record = DB::get_count('users');
        $limit = 5;
        // Get list product records by page
        $offset = ($page - 1) * $limit;
        $list_user= DB::get_list_limit_user($limit, $offset,'users','0');
//        $list_user= User_model1::get_all_user();
        include ('views/list_user.php');
        break;
    default :
        break;
}
