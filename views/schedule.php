<?php include 'views/layout/header.php'; ?>
<div id ="init">
    <form action="<?php echo DOMAIN?>index.php?controller=choice&action=create" method="POST" id="mau">
        <input type="hidden" name="cinema_name" value="" id="cinema_name">
        <input type="hidden" name="id_cinema" value="" id="id_cinema">
        <input type="hidden" name="film_name" value="" id="">
        <input type="hidden" name="id_film" value="" id="id_film">
        <input type="hidden" name="day_name" value="" id="day_name">
        <input type="hidden" name="id_day" value="" id="id_day">
        <input type="hidden" name="time_name" value="" id="">

        <div class="row" id="mau1">
            <div class="col-md-3 box-choice">
                <div class="title-box">
                    <h2>Chọn rạp</h2>
                </div>
                <div class="" id="cinema">
                    <ul>
                        <?php foreach ($list_cinema as $cinema) { ?>
                            <li>
                                <a  href="#" id="<?php echo $cinema['id']; ?>" >
                                    <?php echo $cinema['name'] ?>
                                </a>
                            </li>
                        <?php } ?>
                    </ul>
                    <div id="cn"></div>
                </div>
            </div>
            <div class="col-md-3 box-choice">
                <div class="title-box">
                    <h2>Chọn film</h2>
                </div>
                <div class="" id="film">

                </div>
                <div id="fl"></div>
            </div>
            <div class="col-md-3 box-choice">
                <div class="title-box">
                    <h2>Chọn ngày</h2>
                </div>
                <div class="" id="day">

                </div>
                <div id="ng"></div>
            </div>
            <div class="col-md-3 box-choice">
                <div class="title-box">
                    <h2>Chọn suất</h2>
                </div>
                <div class="" id="time">

                </div>
                <div id="s"></div>
            </div>
        </div>
        <div class=" row col-md-offset-10" style="margin-top: 30px;">
            <div id="number_ticket" style="display: none;">
                <label for="selec">Chọn số vé:</label>
                <select name="number_ticket" id="selec">
                    <?php for($i=0;$i<=10;$i++){?>
                    <option value="<?php echo $i?>"><?php echo $i?></option>
                    <?php } ?>
                </select>
            </div>
        </div>
        <div class="row col-md-offset-10" style="margin-top:  20px;">
            <div id="booking">
                <button type="submit" id="">book</button>
            </div>
        </div>
    </form>
</div>
<div id="confirm" style="display:none">
    <label>Rạp : </label> <span id="rapphim"> 123123</span>
    <form id="confirm-form">

        <input type="hidden" name="rap" value="" id="input-rap">

    </form>
</div>
<script>
    $(document).ready(function () {
        var cinema_id = 1;
        var film_id = 1;
        var day_id = 1;
        var time_id = 1;
//        $("form#mau").submit(function (e) {
//            e.preventDefault();
//        });

        $("#cinema a").on('click', function () {
            $("#cinema ul li a").css('color', 'blue');
            $(this).css('color', 'red');
            cinema_id = $(this).attr('id');
            cinema_name=$(this).text();
            var cn1 = "<input type='hidden' id='' value='" + cinema_name + "' name='cinema_name'>";
                cn1 += "<input type='hidden' id='' value='" + cinema_id + "' name='id_cinema'>";
            $("#cn").html(cn1);
            $("#day,#time").html("");
            $("#number_ticket").hide();
            $.ajax({
                type: 'POST',
                url: 'http://hdcinema.com/cinema/index.php?controller=choice&action=list_films',
                data: {id: $(this).attr('id')},
                dataType: "json",
                success: function (data) {
                    var output = "<ul>";
                    $.each(data, function (index, value) {
                        console.log(value);
                        output += "<li>";
                        output += "<a href='#' id='" + value[0].id + "'>";
                        output += value[0].name;
                        output += "</a>";
                        output += "</li>";
                    });
                    output += "</ul>";


                    $("#film").html(output);
                },
                error: function (xhr, status, error) {
                    alert(error);
//                  console.log(xhr);
                }
            });
        });
        $("#film").on('click', 'a', function () {
            $("#film ul li a").css('color', 'blue');
            $(this).css('color', 'red');
            film_id = $(this).attr('id');
            film_name=$(this).text();
            var fm1 = "<input type='hidden' id='' value='" + film_id + "' name='id_film'>";
                fm1 += "<input type='hidden' id='' value='" + film_name+ "' name='film_name'>";
            $("#fl").html(fm1);
            $("#time").html("");
            $("#number_ticket").hide();
            $.ajax({
                type: 'POST',
                url: 'http://hdcinema.com/cinema/index.php?controller=choice&action=list_days',
                data: {id: $(this).attr('id')},
                dataType: "json",
                success: function (data) {
                    var output = "<ul>";
                    $.each(data, function (index, value) {
                        console.log(value);
                        output += "<li>";
                        output += "<a href='#' id='" + value[0].id + "'>";
                        output += value[0].name;
                        output += "</a>";
                        output += "</li>";
                    });
                    output += "</ul>";
                    $("#day").html(output);
                },
                error: function (xhr, status, error) {
                    alert(error);
                    console.log(xhr);
                }
            });
        });
        $("#day").on('click', 'a', function () {
            $("#day ul li a").css('color', 'blue');
            $(this).css('color', 'red');
            day_id = $(this).attr('id');
            day_name=$(this).text();
            var day1 = "<input type='hidden' id='' value='" + day_id + "' name='id_day'>";
                day1+="<input type='hidden' id='' value='" + day_name + "' name='day_name'>";
            $("#ng").html(day1);
            $("#number_ticket").hide();
            $.ajax({
                type: 'POST',
                url: 'http://hdcinema.com/cinema/index.php?controller=choice&action=list_times',
                data: { id_day: $(this).attr('id'),
                        id_film: film_id},
                dataType: "json",
                success: function (data) {
                    var output = "<ul>";
                    $.each(data, function (index, value) {
                        console.log(value);
                        output += "<li>";
                        output += "<a href='#' id='" + value[0].id + "'>";
                        output += value[0].name;
                        output += "</a>";
                        output += "</li>";
                    });
                    output += "</ul>";
                    $("#time").html(output);
                },
                error: function (xhr, status, error) {
//                 console.log(typeof html);
//                alert(error);
//                alert(xhr.responseText);
                    console.log(xhr);
                }
            });
        });
        $("#time").on('click', 'a', function () {
            $("#time ul li a").css('color', 'blue');
            $(this).css('color', 'red');
            time_id = $(this).attr('id');
            time_name=$(this).text();
            var tm1 = "<input type='hidden' id='' value='" + time_id + "' name='id_time'>";
                tm1+="<input type='hidden' id='' value='" + time_name + "' name='time_name'>";
            $("#s").html(tm1);
            $("#number_ticket").show();
            
            /*$("#booking button").on('click', function () {
                /* $.ajax({
                 type: 'POST',
                 url: 'http://localhost/cinema/index.php?controller=choice&action=create',
                 data: {id_time: $(this).attr('id'),
                 id_film: film,
                 id_cinema: cinema,
                 id_day: day},
                 dataType: "json",
                 success: function (data) {
                 console.log(data);
                 $("div #mau1").hide();
                 //                 var output = "<ul>";
                 //                 $.each(data, function (index, value) {
                 //                     console.log(value);
                 //                      output += "<li>";
                 //                     output +="<a href='#' id='"+value[0].id+"'>";
                 //                     output += value[0].name;
                 //                     output +="</a>";
                 //                     output += "</li>";
                 //                 });
                 //                 output += "</ul>";
                 //                 $("#day").html(output);
                 //                window.location = 'index.php?controller=choice&action=show';
                 //                    alert('abc');
                 },
                 error: function(xhr, status, error) {
                 //                   alert(error);
                 console.log(xhr);
                 }
                 });
                console.log(cinema);
                $("#rapphim").text(cinema);
                $("#mau").hide();
                $("input#input-rap").val(cinema);
                $("#confirm").show();
            });*/
        });
    });
</script>

<?php include 'views/layout/footer.php'; ?>