
<?php include 'views/layout/header_admin.php' ?>
<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content" id="createb" >
        <form method="POST" action="" enctype="multipart/form-data">
        <div class="content-panel" >
                <div class="panel-title">
                    <h2>Change schedule</h2>
                </div>
                <div class="panel-block" style="width: 70%;margin: auto">
                    <div class="row" style="width: 100%">
                    <div class="form-group">
                    <label class="col-md-3 control-label " for="name">Film</label>  
                    <div class="col-md-6">
                        <?php echo $film['name']?>
                    </div>
                    
                    </div>
                    </div>
                    <!--<div class="clearfix"></div>-->
                <!-- Text input-->
                <div class="row" style="width: 100%">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="cinema" >Choose Cinema</label>  
                        <div class="col-md-6">
                            <!--<input id="type" name="typefilm" value="<php echo DB::get_post('typefilm', $film['typefilm'])?>"  type="text" placeholder="" class="form-control input-md">-->
                            <select name="cinema[]"  size="3" multiple="multiple" tabindex="1" id="cinema">
                                <?php foreach ($list_cinema as $cinema) { ?>
                                    <option value="<?php echo $cinema['id']?>"><?php echo $cinema['name']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['cinema'])? $errors['cinema'] :''; ?></span>
                        </div>
                    </div>
                </div>
                     <!--<div class="clearfix"></div>-->
                <!-- Text input-->
                <div class="row" style="width: 100%">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="day">Choose Day</label>  
                        <div class="col-md-6">
                            <!--<input id="actor" name="actor" value="<php echo DB::get_post('actor', $film['actor'])?>"  type="text" placeholder="" class="form-control input-md" >-->
                            <select name="day[]" size="3" multiple="multiple" tabindex="1" id="day">
                                <?php foreach ($list_day as $day) { ?>
                                    <option value="<?php echo $day['id']?>"><?php echo $day['name']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['day'])? $errors['day'] :''; ?></span>
                        </div>
                    </div>
                </div>
                    <!--<div class="clearfix"></div>-->
                <!-- Text input-->
                <div class="row" style="width: 100%">
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="schedule">Choose schedule</label>  
                        <div class="col-md-6">
                            <!--<input id="director" name="director" value="<php echo DB::get_post('director', $film['director'])?>"  type="text" placeholder="" class="form-control input-md" >-->
                            <select name="schedule[]" size="3" multiple="multiple" tabindex="1" id="schedule">
                                <?php foreach ($list_schedule as $schedule) { ?>
                                    <option value="<?php echo $schedule['id']?>"><?php echo $schedule['name']?></option>
                                <?php }?>
                            </select>
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['schedule'])? $errors['schedule'] :''; ?></span>
                        </div>
                    </div>
                </div>
                    <!--<div class="clearfix"></div>-->
                     <div class="row" style="width: 100%">
                    <div class="form-group">
                    <!--<label class="col-md-12 control-label " for="name">Film</label>-->  
                    <div class="col-md-12">
                        <div  style="color: green">
                            <span>*Nhấn Ctrl và click chuột trái để chọn nhiều</span>
                        </div>
                    </div>
                    
                    </div>
                    </div>
                <!-- Button -->
                <div class="form-group" style="margin-top: 20px;">
                        <label class="col-md-4 control-label" ></label>
                        <div class="col-md-4">
                            <button  type="submit" class="btn btn-primary" >Submit</button>
                        </div>
                    </div>
                <div style="color: #28a4c9"> <?php echo isset($message)? $message:''?></div>
                </div>
                
            </div>
        </form>
    </div>
</div>

<?php include 'views/layout/footer_admin.php' ?>