<?php include 'views/layout/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="wrap">
                <p class="form-title">
                    Enter the email you signed up with</p>
                <form class="login" method="POST">
                    <input type="email" name="email" value="<?php echo DB::get_post('email','')?>" placeholder="Email" />
                    <span style="color: red"><?php echo isset($errors['email'])? $errors['email']:''?></span>
                <input type="submit" value="Submit" class="btn btn-success btn-sm" />
                    <!--<p style="color: green;font-size: 14px;"><php echo isset($errors['common'])? $errors['common']:''?></p>-->

                </form>
                <p class="form-title" >
                    <?php echo isset($mes)? $mes : ''?> </p>
            </div>
            
        </div>
    </div>
</div>
<?php include 'views/layout/footer.php'; ?>