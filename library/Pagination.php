<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Pagination
 *
 * @author Administrator
 */
class Pagination {
    
    /**
     * 
     * @param type $total
     * @param type $limit
     * @param type $cur_page
     * @param type $base_url http://domain/index.php?controller=abc&page=
     * @return string
     */
    static function render ($total, $limit, $page, $base_url='') {
        $total_page = ceil($total / $limit);
        $page_view = 3;
        
        $str = '<ul class="pagination">';
        if ( $page - $page_view > 1) {
            $str .= '<li><a href="'.$base_url.'1">First</a></li>';
        }
        
        if ($page > 1) {
            $str .= '<li><a href="'.$base_url.($page - 1).'" >Prev</a></li>';
        }
        
        if ($page - $page_view >= 2) {
            $str .= '<li><a>...</a></li>';
        }
        
        $from_page = $page - $page_view;
        if ($from_page < 1) {
            $from_page = 1;
        }

        $to_page = $page + $page_view;
        if ($to_page > $total_page) {
            $to_page = $total_page;
        }
        for ($i =  $from_page; $i <=  $to_page; ++$i) {
            $str .= '<li '.($page == $i ? 'class="active"' : '' ).' >'
                    . ' <a href="'.$base_url.$i.'">'.$i.'</a></li>';
        }
        
        if ( $total_page - ($page + $page_view) >= 1) {
            $str .= '<li><a>...</a></li>';
        }
        
        if ($page < $total_page) {
            $str .= '<li><a href="'.$base_url.($page + 1).'" >Next</a></li>';
        }
        
        if ($page + $page_view < $total_page) { 
            $str .= '<li><a href="'.$base_url.$total_page.'" >Last</a></li>';
        }
        
        $str .= '</ul>';
        
        return $str;
    }
    
}
