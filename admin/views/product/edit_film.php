
<?php include 'views/layout/header_admin.php' ?>
<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content" id="createb" >
        <form method="POST" action="" enctype="multipart/form-data">
        <div class="content-panel" >
                <div class="panel-title">
                    <h2>Edit Film Information </h2>
                </div>
                <div class="panel-block" style="width: 70%;margin: auto">
                    <div class="form-group">
                    <label class="col-md-3 control-label " for="name">Name</label>  
                    <div class="col-md-6">
                        <input id="username" name="name" value="<?php echo DB::get_post('name', $film['name'])?>" type="text" placeholder="" class="form-control input-md" >
                    </div>
                    <div class="col-md-3" style="color: red">
                        <span>*<?php echo isset($errors['name'])? $errors['name']: '';?></span>
                    </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="type" >Genre</label>  
                        <div class="col-md-6">
                            <input id="type" name="typefilm" value="<?php echo DB::get_post('typefilm', $film['typefilm'])?>"  type="text" placeholder="" class="form-control input-md">

                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['typefilm'])? $errors['typefilm']: '';?></span>
                        </div>
                    </div>
                     <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="actor">Actor</label>  
                        <div class="col-md-6">
                            <input id="actor" name="actor" value="<?php echo DB::get_post('actor', $film['actor'])?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['actor'])? $errors['actor'] : '';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="director">Director</label>  
                        <div class="col-md-6">
                            <input id="director" name="director" value="<?php echo DB::get_post('director', $film['director'])?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['director'])? $errors['director'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="duration">Duration</label>  
                        <div class="col-md-6">
                            <input id="duration" name="duration" value="<?php echo DB::get_post('duration', $film['duration'])?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['duration'])? $errors['duration'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="starttime">Starttime</label>  
                        <div class="col-md-6">
                            <input id="starttime" name="starttime" value="<?php echo DB::get_post('starttime', $film['starttime'])?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['starttime'])? $errors['starttime'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="trailer">Trailer</label>  
                        <div class="col-md-6">
                            <input id="trailer" name="trailer" value="<?php echo DB::get_post('trailer', $film['trailer'])?>"  type="text" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['trailer'])? $errors['trailer'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="introduce">Introduce</label>  
                        <div class="col-md-6">
                            <textarea id="introduce" name="introduce"  placeholder="" class="form-control input-md" ></textarea>
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['introduce'])? $errors['introduce'] :'';?></span>
                        </div>
                    </div>
                <div class="clearfix" style="height: 71px"></div>
                <!-- Text input-->
                    <div class="form-group">
                        <label class="col-md-3 control-label" for="image">Image</label>  
                        <div class="col-md-6">
                            <input  id="image" name="image" value="<?php echo isset($_FILES['image']['name']) ? $_FILES['image']['name'] : ''?>"  type="file" placeholder="" class="form-control input-md" >
                        </div>
                        <div class="col-md-3" style="color: red">
                            <span>*<?php echo isset($errors['image'])? $errors['image'] :'';?></span>
                        </div>
                    </div>
                    <div class="clearfix"></div>
                <!-- Button -->
                    <div class="form-group">
                        <label class="col-md-4 control-label" for=""></label>
                        <div class="col-md-4">
                            <button id="" type="submit" class="btn btn-primary" >Submit</button>
                        </div>
                    </div>
                <div style="color: #28a4c9"> <?php echo isset($message)? $message:''?></div>
                </div>
                
            </div>
        </form>
    </div>
</div>

<?php include 'views/layout/footer_admin.php' ?>