<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <title>Bootstrap 101 Template</title>
        <link href="<?php echo DOMAIN ?>public/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?php echo DOMAIN ?>public/css/style.css" rel="stylesheet">

    </head>
    <body class="container">
        <header class="bg-white-only header header-md navbar navbar-fixed-top-xs">
           

            <div class="main-menu-logo">
                <div class="logo">
                    <a href="http://phimmoi.com"><img src="http://phimmoi.com/wp-content/uploads/2016/02/logo_phimmoi.png"></a>				</div>
                <div class="main-menu">
                    <div id="mega-menu-wrap-primary" class="mega-menu-wrap">
                        <div class="mega-menu-toggle">

                        </div>
                        <ul id="mega-menu-primary" class="mega-menu mega-menu-horizontal" data-event="hover" data-effect="fade" data-second-click="close" data-document-click="collapse" data-reverse-mobile-items="true" data-vertical-behaviour="standard" data-breakpoint="600">
                            <li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-168" id="mega-menu-item-168">
                                <a class="mega-menu-link" href="<?php echo DOMAIN ?>index.php?controller=choice&action=list_cinema">Lịch chiếu</a>

                            </li>
                            <li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-menu-item-has-children mega-align-bottom-left mega-menu-megamenu mega-menu-item-183" id="mega-menu-item-183">
                                <a class="mega-menu-link" href="#">Phim đang chiếu</a>

                            </li>
                            <li class="mega-menu-item mega-menu-item-type-taxonomy mega-menu-item-object-category mega-align-bottom-left mega-menu-flyout mega-menu-item-193" id="mega-menu-item-193">
                                <a class="mega-menu-link" href="#">Phim sắp chiếu</a>
                            </li>
                            <li class="mega-menu-item mega-menu-item-type-taxonomy mega-menu-item-object-category mega-align-bottom-left mega-menu-flyout mega-menu-item-476" id="mega-menu-item-476">
                                <a class="mega-menu-link" href="#">Thành viên</a>
                            </li>
                            <li class="mega-menu-item mega-menu-item-type-post_type mega-menu-item-object-page mega-align-bottom-left mega-menu-flyout mega-menu-item-184" id="mega-menu-item-184">
                                <a class="mega-menu-link" href="#">Rạp phim</a>
                            </li>
                            <li class="mega-menu-item mega-menu-item-type-custom mega-menu-item-object-custom mega-align-bottom-left mega-menu-flyout mega-menu-item-779" id="mega-menu-item-779">
                                <a class="mega-menu-link" href="#">Giải đáp</a>
                            </li>
                        </ul>
                    </div>				
                </div>
            </div>

            <form class="navbar-form navbar-left input-s-lg m-t m-l-n-xs hidden-xs" role="search" method="get" action="http://phimmoi.com">
                <div class="form-group">
                    <div class="input-group">
                        <span class="input-group-btn">
                            <button type="submit" class="btn btn-sm bg-white btn-icon rounded"><i class="fa fa-search"></i></button>
                        </span>
                        <input name="s" type="text" class="form-control input-sm no-border rounded" placeholder="Tìm kiếm phim..." onkeyup="request(this.value);" value="">
                    </div>
                </div>
            </form>

        </header>
        <section>
            <div class="row">
                <div id="myCarousel" class="carousel slide col-md-12" data-ride="carousel">
                    <!-- Indicators -->
                    <ol class="carousel-indicators">
                        <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                        <li data-target="#myCarousel" data-slide-to="1"></li>
                        <li data-target="#myCarousel" data-slide-to="2"></li>
                        <li data-target="#myCarousel" data-slide-to="3"></li>
                    </ol>

                    <!-- Wrapper for slides -->
                    <div class="carousel-inner" role="listbox">
                        <div class="item active">
                            <img src="../resources/default/img/img_system/banner1.png" alt="Chania"  />
                        </div>

                        <div class="item">
                            <img src="../resources/default/img/img_system/banner2.jpg" alt="Chania"  />
                        </div>

                        <div class="item">
                            <img src="../resources/default/img/img_system/banner3.jpg" alt="Flower"  />
                        </div>

                        <div class="item">
                            <img src="../resources/default/img/img_system/banner4.jpg" alt="Flower" />
                        </div>
                    </div>

                    <!-- Left and right controls -->
                    <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
                        <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
                        <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>

            <div class="box-header">
                <h2 class="font-thin m-b">Phim đề cử</h2>
            </div>   
            <div class="row row-sm">
                <div class="carousel slide" data-interval="false" id="slide-blockbuster" style="">
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                <div class="item1">
                                    <div class="pos-rlt">
                                        <div class="top text-right wrapper-sm">
                                            <span class="badge bg-info-year m-l-sm m-b-sm">2016</span>
                                        </div>
                                        <div class="item-overlay opacity bg-black">

                                            <div class="info-box">
                                                <div class="main-title">Huyền Thoại Của Ngày Mai (Phần 1)</div>
                                                <div class="original-title">Legends of Tomorrow (Season 1) (2016)</div>
                                                <div class="time-year">44 phút | 2016</div>
                                                <div class="view">
                                                    2 lượt xem							</div>
                                                <div class="excerpt">
                                                    Năm 2166, tên ác nhân bất tử Vandal Savage (Casper Crump) đã cận kề với thắng lợi cuối cùng của hắn – gây nên sự hỗn loạn và hủy d...							</div>
                                                <a href="http://phimmoi.com/huyen-thoai-cua-ngay-mai-1-291/" class="watch-link">
                                                    <i class="icon-control-play i-2x"></i> Xem ngay</a>
                                            </div>

                                        </div>
                                        <a href="http://phimmoi.com/huyen-thoai-cua-ngay-mai-1-291/">
                                            <i style="background-image: url('http://phimmoi.com/wp-content/uploads/2016/01/legends-of-tomorrow-240x360.jpg');  padding-top: 150%;" alt="Huyền Thoại Của Ngày Mai (Phần 1)" class="img-full"></i></a>
                                    </div>
                                    <div class="title-box">
                                        <a href="http://phimmoi.com/huyen-thoai-cua-ngay-mai-1-291/" class="text-ellipsis">Huyền Thoại Của Ngày Mai (Phần 1)</a>
                                        <a href="http://phimmoi.com/huyen-thoai-cua-ngay-mai-1-291/" class="text-ellipsis text-xs text-muted">Legends of Tomorrow (Season 1) (2016)</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                <div class="item1">
                                    <div class="pos-rlt">
                                        <div class="top text-right wrapper-sm"><span class="badge bg-info-year m-l-sm m-b-sm">2014</span></div>
                                        <div class="item-overlay opacity bg-black">

                                            <div class="info-box">
                                                <div class="main-title">Quả Tim Máu</div>
                                                <div class="original-title">Vengeful Heart (2014)</div>
                                                <div class="time-year">96 phút | 2014</div>
                                                <div class="view">
                                                    0 lượt xem							</div>
                                                <div class="excerpt">
                                                    Chuyện phim kể về chàng rể “chui gầm chạn” tên Sơn hộ tống cô vợ mới cưới là Linh đi nghỉ sau ca phẫu thuật thay tim....							</div>
                                                <a href="http://phimmoi.com/qua-tim-mau-2200/" class="watch-link"><i class="icon-control-play i-2x"></i> Xem ngay</a>
                                            </div>

                                        </div>
                                        <a href="http://phimmoi.com/qua-tim-mau-2200/"><i style="background-image: url('http://phimmoi.com/wp-content/uploads/2016/04/loat-phim-kinh-di-chat-lu-cua-dien-anh-viet-302937-240x360.jpg');  padding-top: 150%;" alt="Quả Tim Máu" class="img-full"></i></a>
                                    </div>
                                    <div class="title-box">
                                        <a href="http://phimmoi.com/qua-tim-mau-2200/" class="text-ellipsis">Quả Tim Máu</a>
                                        <a href="http://phimmoi.com/qua-tim-mau-2200/" class="text-ellipsis text-xs text-muted">Vengeful Heart (2014)</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="item">
                            <div class="col-xs-6 col-sm-4 col-md-3 col-lg-2">
                                <div class="item1">
                                    <div class="pos-rlt">
                                        <div class="top text-right wrapper-sm"><span class="badge bg-info-year m-l-sm m-b-sm">2015</span></div>
                                        <div class="item-overlay opacity bg-black">

                                            <div class="info-box">
                                                <div class="main-title">Diệp Vấn 3</div>
                                                <div class="original-title">Ip Man 3 (2015)</div>
                                                <div class="time-year">104 phút | 2015</div>
                                                <div class="view">
                                                    0 lượt xem							</div>
                                                <div class="excerpt">
                                                    Diệp Vấn 3,&nbsp;Pegasus Motion thông báo tác phẩm võ thuật Diệp Vấn 3 có sự góp mặt của Mike Tyson. Hãng phim đồng thời hứa hẹn khán g...							</div>
                                                <a href="http://phimmoi.com/diep-van-3-1511/" class="watch-link"><i class="icon-control-play i-2x"></i> Xem ngay</a>
                                            </div>

                                        </div>
                                        <a href="http://phimmoi.com/diep-van-3-1511/"><i style="background-image: url('http://phimmoi.com/wp-content/uploads/2016/03/750x1044_movie13454postersip_man_3-hk-240x360.jpg');  padding-top: 150%;" alt="Diệp Vấn 3" class="img-full"></i></a>
                                    </div>
                                    <div class="title-box">
                                        <a href="http://phimmoi.com/diep-van-3-1511/" class="text-ellipsis">Diệp Vấn 3</a>
                                        <a href="http://phimmoi.com/diep-van-3-1511/" class="text-ellipsis text-xs text-muted">Ip Man 3 (2015)</a>
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                    </div>
                    <a class="left carousel-control" href="#slide-blockbuster" data-slide="prev">
                        <i class="fa fa-angle-left"></i>
                    </a>
                    <a class="right carousel-control" href="#slide-blockbuster" data-slide="next">
                        <i class="fa fa-angle-right"></i>
                    </a>
                </div>
            </div>

        </section>
        <footer>

        </footer>
    </body>
</html>