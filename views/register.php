<?php include 'views/layout/header.php'; ?>
<div class="container">
    <div class="row">
        <div class="col-md-12">
            
            <div class="wrap">
                <p class="form-title">
                    Sign Up</p>
                <form class="register" method="POST" >
                    <input type="text" name="username" value="<?php echo DB::get_post('username', '')?>" placeholder="Username" />
                    <span><?php echo isset($errors['username'])? $errors['username']:''?></span>
                    <input type="password" name="password" value="<?php echo DB::get_post('password', '')?>" placeholder="Password" />
                    <span><?php echo isset($errors['password'])? $errors['password']:''?></span>
                    <input type="password" name="repassword" value="<?php echo DB::get_post('repassword', '')?>" placeholder="Re-Password" />
                    <span><?php echo isset($errors['repassword'])? $errors['repassword']:''?></span>
                    <input type="email" name="email" value="<?php echo DB::get_post('email', '')?>" placeholder="email" />
                    <span><?php echo isset($errors['email'])? $errors['email']:''?></span>
                    <input type="submit" value="Sign In" class="btn btn-success btn-sm" />
                    <div class="remember-forgot" style="margin-bottom: 20px;">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" />
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-md-6 forgot-pass-content">
                            <a href="<?php echo DOMAIN?>index.php?controller=auth&action=login" class="forgot-pass">Have account? Login here</a>
                        </div>
                    </div>
                </div>
                </form>
                <p class="form-title" style="width: 400px;margin: auto" id="mes">
                    <?php echo isset($mes)? $mes:''?>
                </p>
                <p class="form-title" style="width: 400px;margin: auto">
                    <a href="<?php echo DOMAIN?>index.php?controller=auth&action=login" id="log" class="btn btn-primary" style="display: none">Login</a>
                </p>
            </div>
        </div>
    </div>
    
</div>
<script>
        $(document).ready(function (){
           if(($("#mes").text()!='')&&($("span").text()=='')){
               $("a#log").show();
           } 
        });
</script>
<?php include 'views/layout/footer.php'; ?>