CREATE DATABASE  IF NOT EXISTS `hdcinema` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `hdcinema`;
-- MySQL dump 10.13  Distrib 5.6.24, for Win32 (x86)
--
-- Host: localhost    Database: hdcinema
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `advertisments`
--

DROP TABLE IF EXISTS `advertisments`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `advertisments` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `description` varchar(45) DEFAULT NULL,
  `views` int(100) DEFAULT NULL,
  `link` varchar(45) DEFAULT NULL,
  `hide` int(1) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `advertisments`
--

LOCK TABLES `advertisments` WRITE;
/*!40000 ALTER TABLE `advertisments` DISABLE KEYS */;
/*!40000 ALTER TABLE `advertisments` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `bookings`
--

DROP TABLE IF EXISTS `bookings`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `bookings` (
  `id` int(10) NOT NULL,
  `content` varchar(45) DEFAULT NULL,
  `film_id` int(10) DEFAULT NULL,
  `user_id` int(10) DEFAULT NULL,
  `quantity` int(10) DEFAULT NULL,
  `schedule` varchar(45) DEFAULT NULL,
  `date` varchar(45) DEFAULT NULL,
  `seat` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `bookings_film_id_foreign_idx` (`film_id`),
  KEY `bookings_user_id_foreign_idx` (`user_id`),
  CONSTRAINT `bookings_film_id_foreign` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `bookings_user_id_foreign` FOREIGN KEY (`user_id`) REFERENCES `users` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `bookings`
--

LOCK TABLES `bookings` WRITE;
/*!40000 ALTER TABLE `bookings` DISABLE KEYS */;
/*!40000 ALTER TABLE `bookings` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `categories`
--

DROP TABLE IF EXISTS `categories`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `categories` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `categories`
--

LOCK TABLES `categories` WRITE;
/*!40000 ALTER TABLE `categories` DISABLE KEYS */;
INSERT INTO `categories` VALUES (1,'phim dang chieu'),(2,'phim sap chieu');
/*!40000 ALTER TABLE `categories` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `cinemas`
--

DROP TABLE IF EXISTS `cinemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `cinemas` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `cinemas`
--

LOCK TABLES `cinemas` WRITE;
/*!40000 ALTER TABLE `cinemas` DISABLE KEYS */;
INSERT INTO `cinemas` VALUES (1,'da nang','12 hung vuong','123456789','abc'),(2,'hue','23 tran phu','123456789','abc'),(3,'quang nam','54 phan chu trinh','11234567','abc');
/*!40000 ALTER TABLE `cinemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `day_film`
--

DROP TABLE IF EXISTS `day_film`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `day_film` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `day_id` int(10) DEFAULT NULL,
  `film_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `day_film_film_id_foreign_idx` (`film_id`),
  KEY `day_film_day_id_foreign_idx` (`day_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `day_film`
--

LOCK TABLES `day_film` WRITE;
/*!40000 ALTER TABLE `day_film` DISABLE KEYS */;
INSERT INTO `day_film` VALUES (1,1,1),(2,1,2),(3,1,3),(4,1,4),(5,2,1),(6,2,2),(7,2,4);
/*!40000 ALTER TABLE `day_film` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `days`
--

DROP TABLE IF EXISTS `days`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `days` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `days`
--

LOCK TABLES `days` WRITE;
/*!40000 ALTER TABLE `days` DISABLE KEYS */;
INSERT INTO `days` VALUES (1,'26/4/2016'),(2,'27/4/2016');
/*!40000 ALTER TABLE `days` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `film_cinemas`
--

DROP TABLE IF EXISTS `film_cinemas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `film_cinemas` (
  `id` int(100) NOT NULL AUTO_INCREMENT,
  `film_id` int(10) DEFAULT NULL,
  `cinema_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `film_cinemas_film_id_foreign_idx` (`film_id`),
  KEY `film_cinemas_cinema_id_foreign_idx` (`cinema_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `film_cinemas`
--

LOCK TABLES `film_cinemas` WRITE;
/*!40000 ALTER TABLE `film_cinemas` DISABLE KEYS */;
INSERT INTO `film_cinemas` VALUES (1,1,1),(2,2,1),(3,3,1),(4,4,1),(5,1,2),(6,2,2),(7,3,2);
/*!40000 ALTER TABLE `film_cinemas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `films`
--

DROP TABLE IF EXISTS `films`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `films` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `typefilm` varchar(45) DEFAULT NULL,
  `actor` varchar(255) DEFAULT NULL,
  `director` varchar(45) DEFAULT NULL,
  `duration` varchar(45) DEFAULT NULL,
  `starttime` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `views` int(255) DEFAULT NULL,
  `hide` int(1) DEFAULT NULL,
  `category_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `films_category_id_idx` (`category_id`),
  CONSTRAINT `films_category_id_foreign` FOREIGN KEY (`category_id`) REFERENCES `categories` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `films`
--

LOCK TABLES `films` WRITE;
/*!40000 ALTER TABLE `films` DISABLE KEYS */;
INSERT INTO `films` VALUES (1,'jungle book','phieu luu','abc','abc','1:30:00','25/4/2016','anh1.jpg',1,NULL,1),(2,'asian park','phieu luu','abc','abc','1:30:00','25/4/2016','anh2.jpg',1,NULL,2),(3,'iron man','vien tuong','abc','abc','1:30:00','25/4/2016','anh3.jpg',1,NULL,1),(4,'spider man','vien tuong','abc','abc','1:30:00','25/4/2016','anh4.jpg',1,NULL,2);
/*!40000 ALTER TABLE `films` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `images`
--

DROP TABLE IF EXISTS `images`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `images` (
  `id` int(10) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  `cinema_id` int(10) DEFAULT NULL,
  `film_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `images_cinema_id_foreign_idx` (`cinema_id`),
  KEY `images_film_id_foreign_idx` (`film_id`),
  CONSTRAINT `images_cinema_id_foreign` FOREIGN KEY (`cinema_id`) REFERENCES `cinemas` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION,
  CONSTRAINT `images_film_id_foreign` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `images`
--

LOCK TABLES `images` WRITE;
/*!40000 ALTER TABLE `images` DISABLE KEYS */;
/*!40000 ALTER TABLE `images` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `news`
--

DROP TABLE IF EXISTS `news`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `news` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) DEFAULT NULL,
  `link` varchar(45) DEFAULT NULL,
  `views` int(10) DEFAULT NULL,
  `hide` int(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `news`
--

LOCK TABLES `news` WRITE;
/*!40000 ALTER TABLE `news` DISABLE KEYS */;
/*!40000 ALTER TABLE `news` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `schedules`
--

DROP TABLE IF EXISTS `schedules`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `schedules` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `film_id` int(10) DEFAULT NULL,
  `room` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `schedule_film_id_foreign_idx` (`film_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `schedules`
--

LOCK TABLES `schedules` WRITE;
/*!40000 ALTER TABLE `schedules` DISABLE KEYS */;
INSERT INTO `schedules` VALUES (1,'21:00',1,'1'),(2,'10:00',2,'2'),(3,'14:00',2,'2'),(4,'21:00',2,'2'),(5,'8:00',3,'3'),(6,'13:00',3,'3'),(7,'20:00',3,'3'),(8,'9:30',4,'4'),(9,'13:30',4,'4'),(10,'17:00',4,'4'),(11,'22:00',4,'4'),(15,'9:00',1,'1'),(16,'11:00',1,'1'),(17,'14:00',1,'1');
/*!40000 ALTER TABLE `schedules` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `seats`
--

DROP TABLE IF EXISTS `seats`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `seats` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `content` varchar(255) DEFAULT NULL,
  `schedule_id` int(10) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `seats_schedule_id_foreign_idx` (`schedule_id`)
) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `seats`
--

LOCK TABLES `seats` WRITE;
/*!40000 ALTER TABLE `seats` DISABLE KEYS */;
INSERT INTO `seats` VALUES (1,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',1),(2,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',2),(3,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',3),(4,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',4),(5,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',5),(6,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',6),(7,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',7),(8,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',8),(9,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',9),(10,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',10),(11,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',11),(12,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',12),(13,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',13),(14,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',14),(15,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',15),(16,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',16),(17,'a1,a2,a3,a4,a5,b1,b2,b3,b4,b5,c1,c2,c3,c4,c5',17);
/*!40000 ALTER TABLE `seats` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `users` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `name` varchar(45) DEFAULT NULL,
  `tel` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `block` int(1) DEFAULT NULL,
  `image` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `videos`
--

DROP TABLE IF EXISTS `videos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `videos` (
  `id` int(10) NOT NULL,
  `name` varchar(45) DEFAULT NULL,
  `film_id` int(10) DEFAULT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `videos_film_id_foreign_idx` (`film_id`),
  CONSTRAINT `videos_film_id_foreign` FOREIGN KEY (`film_id`) REFERENCES `films` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `videos`
--

LOCK TABLES `videos` WRITE;
/*!40000 ALTER TABLE `videos` DISABLE KEYS */;
/*!40000 ALTER TABLE `videos` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-04-25 17:31:58
