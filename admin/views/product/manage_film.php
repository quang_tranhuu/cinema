<?php include 'views/layout/header_admin.php' ?>

<div class="col-md-9" style="padding-right:   0px ;">
    <div class="profile-content">
        <div class="row">
        <div class="col-md-8">
        <h1>MANAGE FILMS</h1>
    </div>
    
    <div class="col-md-12">
        <table class="table table-hover"> 
            <thead> 
                <tr> 
                    <th>Index</th> 
                    <th>Film name</th> 
                    <th>Type</th> 
                    <th>Duration</th> 
                    <th>Views</th> 
                    <th>Time</th>
                    <th>Action</th>
                </tr> 
            </thead> 
            <tbody> 
                <?php foreach($list_films as  $film) { ?>
                <tr> 
                    <th scope="row"><?php echo ++$offset; ?></th> 
                    <td><?php echo $film['name'] ?></td>
                    <td><?php echo $film['typefilm'] ?></td>
                    <td><?php echo $film['duration'] ?></td>
                    <td><?php echo $film['views'] ?></td>
                    <td><?php echo $film['starttime'] ?></td>
                    <td>
                        <a href="<?php echo DOMAIN_AD .'index.php?controller=product&action=edit&id=' . $film['id'] ?>"><i class="fa fa-pencil-square-o "></i></a> &nbsp;
                        <a href="<?php echo DOMAIN_AD .'index.php?controller=product&action=delete&id=' . $film['id'] ?>"><i class="fa fa-trash"></i></a> 
                        <a href="<?php echo DOMAIN_AD .'index.php?controller=product&action=change_schedule&id=' . $film['id'] ?>"><i class="fa fa-hourglass-half"></i></a> 

                    </td>
                </tr> 
                <?php } ?>
            </tbody> 
        </table>
    </div>
    <div class="col-md-12">
        <div class="col-md-6">
        <nav>
            <?php echo Pagination::render($total_record, $limit, $page, DOMAIN . 'admin/index.php?controller=product&action=manage_film&page='); ?>
        </nav>
        </div>
        <div class="col-md-offset-2 col-md-4 add">
            <a  href="<?php echo DOMAIN .'admin/index.php?controller=product&action=create'; ?>" class="btn btn-primary "><i class="fa fa-plus-circle fa-">Tạo film mới</i></a>
        </div>
    </div> 
        </div>
    </div>
</div>
<?php include 'views/layout/footer_admin.php' ?>