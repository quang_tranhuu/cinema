<?php
require_once ('../models/Booking_model.php');
require_once ('models/User_model1.php');
switch ($action) {
    case 'list_booking':
        require_once '../library/Pagination.php';
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        
        // Get number of total product record in DB
        $total_record = DB::get_count_where('bookings','wait');
        $limit = 5;
        // Get list product records by page
        $offset = ($page - 1) * $limit;
        $list_booking = DB::get_list_limit_record($limit, $offset,'bookings','wait');
        include('views/product/listbooking.php');
        break;
    case 'reject':
        $errors=[];
        
        if(isset($_POST)&&  count($_POST)>0){
            if(DB::get_post('reason', '')==''){
                $errors['reason']='Require';
            }
        
        if(empty($errors)){
            $item= Booking_model::get_booking_by_id((int)$_GET['id']);
            $user=User_model1::get_user_by_id($item['id_user']);
            $des= $user[0]['email'];
            print_r($des);
                $title='Reject Booking';
                $content='Booking của bạn đã bị từ chối vì lí do :'.$_POST['reason'];
                DB::sendMail($des, $content, $title);
                $mesage='Đã gửi !';
            DB::query("UPDATE bookings SET status='rejected' WHERE id=".$_GET['id']);
            }
        }
        include ('views/product/rejectbooking.php');
        break;
    case 'accept':
        DB::query("UPDATE bookings SET status='accepted' WHERE id=".$_GET['id']);
        header("Location: " . DOMAIN . 'admin/index.php?controller=booking&action=list_booking');
        break;
    case 'checked':
        require_once '../library/Pagination.php';
        $page = isset($_GET['page']) ? (int) $_GET['page'] : 1;
        
        // Get number of total product record in DB
        $total_record = DB::get_count_where('bookings','accepted');
        $limit = 5;
        // Get list product records by page
        $offset = ($page - 1) * $limit;
        $list_booking = DB::get_list_limit_record($limit, $offset,'bookings','accepted');
        include('views/product/booking_checked.php');
        break;
        
    
}
